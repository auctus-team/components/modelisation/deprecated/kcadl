#!/usr/bin/env bash

stage_welcome()
{
  print_bold $1 "This program will install the kcadl project on your computer."
  sudo updatedb
}

stage_prerequisite()
{
  print_stage $1 $2 "Installing required packages..."
  sudo apt install -y python python2.7 gcc g++ make pkg-config zlib1g-dev qt5-default libqt5svg5-dev cmake git python-pip python-matplotlib python-numpy python-scipy libgmp3-dev libmpfr-dev libboost-all-dev
  #pip uninstall matplotlib && pip install matplotlib==2.0.2 --user
  sudo apt install -y python3-pip python-numpy python-scipy python-matplotlib
  pip install vtk numpy-stl
}

stage_gaol_install()
{
  print_stage $1 $2 "Installing gaol..."
  sudo sh -c 'echo "https://gitlab.univ-nantes.fr/goualard-f/gaol.git"'
  mkdir -p $KCADL_3RD
  cd $KCADL_3RD
  if [[ ! -e gaol ]]; then
    git clone https://gitlab.univ-nantes.fr/goualard-f/gaol.git $GAOL
    cd $GAOL
    echo $GAOL
    ./configure --prefix=$GAOL/build --enable-preserve-rounding=yes
    make
    make install
  else 
    print_stage $1 $2 "$GAOL already installed"
  fi
}

stage_ibex_install()
{
  print_stage $1 $2 "Installing ibex..."
  sudo sh -c 'echo "https://www.ibex-lib.org/$IBEX.tgz"'
  mkdir -p $KCADL_3RD
  cd $KCADL_3RD
  if [[ ! -e ibex-lib ]]; then
    wget https://github.com/ibex-team/ibex-lib/archive/ibex-2.8.7.tar.gz --no-check-certificate
    tar xvzf ibex-2.8.7.tar.gz
    rm ibex-2.8.7.tar.gz
    mv ibex-lib-ibex-2.8.7 ibex-lib
    cd ibex-lib
    ./waf --prefix=./build configure --lp-lib=soplex --interval-lib=filib # filib required to use CGAL due to rounding mode changes
    ./waf install
  else 
    print_stage $1 $2 "$GAOL already installed"
  fi
}

stage_eigen_install()
{
  print_stage $1 $2 "Installing eigen..."
  sudo sh -c 'echo "http://bitbucket.org/eigen/eigen/get/3.3.7.tar.bz2"'
  sudo apt-get install -y python2.7 flex bison gcc g++ make pkg-config
  mkdir -p $KCADL_3RD
  cd $KCADL_3RD
  if [[ ! -e eigen ]]; then
    wget http://bitbucket.org/eigen/eigen/get/3.3.7.tar.bz2 --no-check-certificate
    tar -xvjf 3.3.7.tar.bz2
    mv eigen-eigen-323c052e1731 eigen
    rm 3.3.7.tar.bz2
    cd eigen
    mkdir build && cd build
    cmake ../ -DCMAKE_INSTALL_PREFIX=./
    make install
  else 
    print_stage $1 $2 "eigen already installed"
  fi
}

stage_qhull_install()
{
  print_stage $1 $2 "Installing qhull..."
  sudo sh -c 'echo "http://www.qhull.org/download/qhull-2019-src-7.3.2.tgz"'
  mkdir -p $KCADL_3RD
  cd $KCADL_3RD
  if [[ ! -e qhull ]]; then
    wget http://www.qhull.org/download/qhull-2019-src-7.3.2.tgz --no-check-certificate
    tar -zxvf qhull-2019-src-7.3.2.tgz
    mv qhull-2019.1 qhull
    rm qhull-2019-src-7.3.2.tgz
    cd qhull
    make M32=
    export LD_LIBRARY_PATH=$PWD/lib:$LD_LIBRARY_PATH
  else 
    print_stage $1 $2 "eigqhullen already installed"
  fi
}

stage_vibes_install()
{
  print_stage $1 $2 "Installing $VIBES..."
  sudo sh -c 'echo "https://github.com/ENSTABretagneRobotics/VIBES.git"'
  if [[ ! -e $VIBES ]]; then
    mkdir -p $KCADL_3RD
    cd $KCADL_OPT
    git clone https://github.com/ENSTABretagneRobotics/VIBES.git $VIBES
    cd $VIBES/viewer
    mkdir build && cd build
    cmake -DCMAKE_INSTALL_PREFIX=distrib ..
    make
    make install
  else 
    print_stage $1 $2 "$VIBES already installed"
  fi
}

stage_cgal_install()
{
    print_stage $1 $2 "Installing $CGAL..."
    if [[ ! -e CGAL-4.13.1 ]]; then
        cd $KCADL_3RD
        wget https://github.com/CGAL/cgal/releases/download/releases%2FCGAL-4.13.1/CGAL-4.13.1.tar.xz --no-check-certificate
        tar xvf CGAL-4.13.1.tar.xz
        rm CGAL-4.13.1.tar.xz
        cd CGAL-4.13.1
        cmake .
        make
        make install
    else 
        print_stage $1 $2 "$CGAL already installed"
    fi   
}

stage_doxygen_install()
{
  print_stage $1 $2 "Installing $DOXYGEN..."
  sudo sh -c 'echo "https://github.com/doxygen/doxygen.git"'
  if [[ ! -e $DOXYGEN ]]; then
    mkdir -p $KCADL_3RD
    cd $KCADL_3RD
    git clone https://github.com/doxygen/doxygen.git $DOXYGEN
    cd $DOXYGEN
    mkdir build
    cd build
    cmake -G "Unix Makefiles" ..
    sudo make 
    sudo make install
  else 
    print_stage $1 $2 "$DOXYGEN already installed"
  fi    
}

stage_make_doxygen()
{
  print_stage $1 $2 "Preparing documentation..."
  cd $FOLDER_START
  mkdir -p doc
  doxygen doc/doxygen.conf
  mv html doc/html
  mv latex doc/latex
}

update_LD_LIBRARY_PATH()
{
  print_stage $1 $2 "Updating LD_LIBRARY_PATH in ~/.bashrc"
  echo 'LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/kcadl/3rd/CGAL-4.13.1/build/lib/' >> ~/.bashrc 
}

print_bold()
{
    echo -e "$(tput bold)$1$(tput sgr0)"
}

print_stage()
{
  print_bold "\n[$1/$2] $3"
}

prompt_str()
{
    read -e -p "$(tput setaf 3)$1:$(tput sgr0) " str
    echo $str
}

prompt_str_pass()
{
    read -s -e -p "$(tput setaf 3)$1:$(tput sgr0) " str
    echo $str
}

echo "###################"
echo "# KCADL INSTALLER #"
echo "###################"
echo ""

STAGE_COUNT=10

PROJECT_NAME="kcadl"
FOLDER_START=$(pwd)
KCADL_3RD="/opt/kcadl/3rd/"
KCADL_BIN="$FOLDER_START/bin/"
KCADL_DOC="$FOLDER_START/doc/"
IBEX=$KCADL_3RD$IBEX_VERSION
VIBES=$KCADL_3RD"vibes"
GAOL=$KCADL_3RD"gaol"
CGAL=$KCADAL_3RD"CGAL-4.13.1"
EIGEN3=$KCADL_3RD"eigen3"
DOXYGEN=$KCADAL_3RD"doxygen"
BASHRC_PATH=$HOME"/.bashrc"
BASHRC_PATH_NEW=$BASHRC_PATH".tmp"
DISTRO="melodic"

stage_welcome
stage_prerequisite 1 $STAGE_COUNT
stage_eigen_install 2 $STAGE_COUNT
#stage_gaol_install 3 $STAGE_COUNT
stage_ibex_install 4 $STAGE_COUNT
stage_qhull_install 5 $STAGE_COUNT
stage_vibes_install 6 $STAGE_COUNT
stage_cgal_install 7 $STAGE_COUNT
stage_doxygen_install 8 $STAGE_COUNT
stage_make_doxygen 9 $STAGE_COUNT
update_LD_LIBRARY_PATH 10 $STAGE_COUNT
echo -e "\nInstallation finished !\n"



