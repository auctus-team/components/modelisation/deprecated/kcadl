/*
 * Porta_6_dof_manipulator_IK.cpp
 *
 *  Created on: Dec 18, 2018
 *  Author: Joshua Pickard
 */

#include "kcadl.h"

int main() {
	bool verbose = true;

	// Load minibex
//	ibex::System system("examples/Inverse-Kinematics/minibex/Porta_6_dof_manipulator_IK.mbx");
	ibex::System system("examples/Inverse-Kinematics/minibex/Porta_6_dof_manipulator_with_ee_IK.mbx");
	std::cout << system << std::endl;

	// building contractors
	ibex::CtcHC4 hc4(system,0.01);
	ibex::CtcHC4 hc4_2(system,0.1,true);
	ibex::CtcAcid acid(system, hc4_2);
	ibex::CtcCompo compo(hc4,acid);

	// Create a smear-function bisection heuristic
	ibex::SmearSumRelative bisector(system, 1e-02);

	// Create a "stack of boxes" (CellStack) (depth-first search)
	ibex::CellStack buff;

	// Vector precisions required on variables
	ibex::Vector prec(6, 1e-02);

	// Create a solver
	ibex::Solver s(system, compo, bisector, buff, prec, prec);

	// Run the solver
	s.solve(system.box);
	const ibex::CovList& cov = s.get_data();

	// Display the solutions
	if (verbose) std::cout << "Solutions" << std::endl;
	if (verbose) std::cout << cov.size() << std::endl;
	std::vector<ibex::IntervalVector> solverSolutions;
	for (size_t i=0; i<cov.size(); i++) {
		solverSolutions.push_back(cov[i]);
	}

	for (int sol=0; sol<solverSolutions.size(); sol++) {
		// Structure to save solutions
		std::cout << "#############################" << std::endl;
		std::cout << "Solution: " << sol << std::endl;
		ibex::IntervalVector box = solverSolutions[sol];

		// Generate transformation matrices
		int n = 5;
		for (int i=0; i<n; i++){
			ibex::Interval Rux = box[i];
			ibex::Interval Ruy = box[i+n];
			ibex::Interval Ruz = box[i+2*n];
			ibex::Interval Rvx = box[i+3*n];
			ibex::Interval Rvy = box[i+4*n];
			ibex::Interval Rvz = box[i+5*n];
			ibex::Interval Rwx = box[i+6*n];
			ibex::Interval Rwy = box[i+7*n];
			ibex::Interval Rwz = box[i+8*n];
			ibex::Interval rx = box[i+9*n];
			ibex::Interval ry = box[i+10*n];
			ibex::Interval rz = box[i+11*n];
			ibex::IntervalMatrix Transform(4,4);
			Transform[0][0] = Rux;
			Transform[1][0] = Ruy;
			Transform[2][0] = Ruz;
			Transform[0][1] = Rvx;
			Transform[1][1] = Rvy;
			Transform[2][1] = Rvz;
			Transform[0][2] = Rwx;
			Transform[1][2] = Rwy;
			Transform[2][2] = Rwz;
			Transform[0][3] = rx;
			Transform[1][3] = ry;
			Transform[2][3] = rz;
			Transform[3][0] = 0;
			Transform[3][1] = 0;
			Transform[3][2] = 0;
			Transform[3][3] = 1;
			std::cout << "T" << 0 << ',' << i+1 << std::endl;
			std::cout << Transform << std::endl;
		}
	}

	/* Report performances */
	std::cout << "cpu time used=" << s.get_time() << "s."<< std::endl;
	std::cout << "number of cells=" << s.get_nb_cells() << std::endl;
	return 0;
}







