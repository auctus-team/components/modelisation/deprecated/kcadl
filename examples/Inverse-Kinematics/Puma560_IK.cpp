/*
 * Puma560_IK.cpp
 *
 *  Created on: Nov 23, 2018
 *  Author: Joshua Pickard
 *
 *  TODO: NOT WORKING
 */

#include "kcadl.h"

using namespace kcadl;

int main() {
	// Puma 560
	double a2 = 300;
	double a3 = 50;
	double d3 = 50;
	double d4 = 200;

	// Create segments
	Segment segmentBase(Joint(Joint::JointType::Base),Frame::Identity());
	Segment segment1(Joint(Joint::JointType::Revolute),Frame::modifiedDH(0.0,0.0,0.0,0.0));
	Segment segment2(Joint(Joint::JointType::Revolute),Frame::modifiedDH(0.0,-M_PI/2,0.0,0.0));
	Segment segment3(Joint(Joint::JointType::Revolute),Frame::modifiedDH(a2,0.0,d3,0.0));
	Segment segment4(Joint(Joint::JointType::Revolute),Frame::modifiedDH(a3,-M_PI/2,d4,0.0));
	Segment segment5(Joint(Joint::JointType::Revolute),Frame::modifiedDH(0.0,M_PI/2,0.0,0.0));
	Segment segment6(Joint(Joint::JointType::Revolute),Frame::modifiedDH(0.0,-M_PI/2,0.0,0.0));

	// Build kinematic chain
	KinematicChain kinematicChain;
	kinematicChain.verbose = false; // Do not display debugging comments
	kinematicChain.addSegment(segmentBase);
	kinematicChain.addSegment(segment1);
	kinematicChain.addSegment(segment2);
	kinematicChain.addSegment(segment3);
	kinematicChain.addSegment(segment4);
	kinematicChain.addSegment(segment5);
	kinematicChain.addSegment(segment6);

	// Desired Poses
	ibex::IntervalMatrix rotationMatrix = ibex::Interval(-1,1)*ibex::IntervalMatrix(ibex::Matrix::ones(3));
	ibex::IntervalVector positionVector(3);
	positionVector[0] = ibex::Interval(200);
	positionVector[1] = ibex::Interval(200);
	positionVector[2] = ibex::Interval(100);

	// Find all IK solutions
	std::vector<KinematicChain::KinematicsSolution> solutions = kinematicChain.solveIK(positionVector, rotationMatrix);
	for (int sol=0; sol<solutions.size(); sol++){
		ibex::IntervalVector jointvalue = solutions[sol].jointValues;
		std::cout << "eePosition: " << solutions[sol].eePosition << std::endl;
		std::cout << "eeOrientation: " << solutions[sol].eeOrientation << std::endl;
		for (int jointNum=0; jointNum<jointvalue.size(); jointNum++){
			ibex::Interval theta = jointvalue[jointNum];
			std::cout << "theta: " << jointNum+1 << theta << "rads\t" << theta*180/M_PI << "degs" << std::endl;
		}
	}
	std::cout << " Finished" << std::endl;
	return 0;

}

