/*
 * UR5_FK.cpp
 *
 *  Created on: Nov 30, 2018
 *  Author: Joshua Pickard
 */

#include "kcadl.h"

using namespace kcadl;

int main() {
	// Universal Robot UR5 manipulator
	ibex::Interval l1 = 0.425;
	ibex::Interval l2 = 0.392;
	ibex::Interval w1 = 0.109;
	ibex::Interval w2 = 0.082;
	ibex::Interval h1 = 0.089;
	ibex::Interval h2 = 0.095;

	// Create twists (screw axes in base frame)
	Twist twist1(0,0,0,0,0,1);
	Twist twist2(-h1,0,0,0,1,0);
	Twist twist3(-h1,0,l1,0,1,0);
	Twist twist4(-h1,0,l1+l2,0,1,0);
	Twist twist5(-w1,l1+l2,0,0,0,-1);
	Twist twist6(h2-h1,0,l1+l2,0,1,0);

	// Create segments
	Segment segmentBase(Joint(Joint::JointType::Base),Frame::Identity());
	Segment segment1(Joint(Joint::JointType::Revolute),Frame::From_twist(twist1));
	Segment segment2(Joint(Joint::JointType::Revolute),Frame::From_twist(twist2));
	Segment segment3(Joint(Joint::JointType::Revolute),Frame::From_twist(twist3));
	Segment segment4(Joint(Joint::JointType::Revolute),Frame::From_twist(twist4));
	Segment segment5(Joint(Joint::JointType::Revolute),Frame::From_twist(twist5));
	Segment segment6(Joint(Joint::JointType::Revolute),Frame::From_twist(twist6));

	// Build kinematic chain
	KinematicChain kinematicChain;
	kinematicChain.verbose = false; // Do not display debugging comments
	kinematicChain.addSegment(segmentBase);
	kinematicChain.addSegment(segment1);
	kinematicChain.addSegment(segment2);
	kinematicChain.addSegment(segment3);
	kinematicChain.addSegment(segment4);
	kinematicChain.addSegment(segment5);
	kinematicChain.addSegment(segment6);

	// Zero transform
	ibex::IntervalMatrix rotationMatrix = ibex::Matrix::eye(3);
	ibex::IntervalVector positionVector(3);
	positionVector[0] = l1 + l2;
	positionVector[1] = w1 + w2;
	positionVector[2] = h1 - h2;
	kinematicChain.zeroTransform(positionVector, rotationMatrix);

	// Find all FK solutions - given joint values
	ibex::IntervalVector jointValues(6);
	jointValues[0] = 0;
	jointValues[1] = -M_PI/2;
	jointValues[2] = 0;
	jointValues[3] = 0;
	jointValues[4] = M_PI/2;
	jointValues[5] = 0;
	std::vector<KinematicChain::KinematicsSolution> solutions1 = kinematicChain.solveFK(jointValues);

	// Display FK results
	for (int index=0; index<solutions1.size(); index++){
		ibex::IntervalMatrix T = Frame::Position_and_orientation(solutions1[index].eePosition,solutions1[index].eeOrientation).TransformationMatrix();
		std::cout << "Homogenous Transform\n" << T << std::endl;
	}

	std::cout << " Finished" << std::endl;
	return 0;


}

