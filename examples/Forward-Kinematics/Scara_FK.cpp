/*
 * Scara_FK.cpp
 *
 *  Created on: Nov 30, 2018
 *  Author: Joshua Pickard
 */

#include "kcadl.h"

using namespace kcadl;

int main() {
	// Scara manipulator
	ibex::Interval l0 = 0.05;
	ibex::Interval l1 = 0.1;
	ibex::Interval l2 = 0.2;

	// Create twists (screw axes in base frame)
	Twist twist1(0,0,0,0,0,1);
	Twist twist2(0,l1,0,0,0,1);
	Twist twist3(0,l1+l2,0,0,0,1);
	Twist twist4(0,0,1,0,0,0);

	// Create segments
	Segment segmentBase(Joint(Joint::JointType::Base),Frame::Identity());
	Segment segment1(Joint(Joint::JointType::Revolute),Frame::From_twist(twist1));
	Segment segment2(Joint(Joint::JointType::Revolute),Frame::From_twist(twist2));
	Segment segment3(Joint(Joint::JointType::Revolute),Frame::From_twist(twist3));
	Segment segment4(Joint(Joint::JointType::Revolute),Frame::From_twist(twist4));

	// Build kinematic chain
	KinematicChain kinematicChain;
	kinematicChain.verbose = false; // Do not display debugging comments
	kinematicChain.addSegment(segmentBase);
	kinematicChain.addSegment(segment1);
	kinematicChain.addSegment(segment2);
	kinematicChain.addSegment(segment3);
	kinematicChain.addSegment(segment4);

	// Zero transform
	ibex::IntervalMatrix rotationMatrix = ibex::Matrix::eye(3);
	ibex::IntervalVector positionVector(3);
	positionVector[0] = 0;
	positionVector[1] = l1+l2;
	positionVector[2] = l0;
	kinematicChain.zeroTransform(positionVector, rotationMatrix);

	// Find FK solutions - given joint values
	ibex::IntervalVector jointValues(4);
	jointValues[0] = 0;
	jointValues[1] = 0;
	jointValues[2] = 0;
	jointValues[3] = 0;
	bool use_filtering = true;
	bool base_reference = true;
	std::vector<KinematicChain::KinematicsSolution> solutions1 = kinematicChain.solveFK(jointValues, use_filtering, base_reference);

	// Display FK results
	for (int index=0; index<solutions1.size(); index++){
		ibex::IntervalMatrix T = Frame::Position_and_orientation(solutions1[index].eePosition,solutions1[index].eeOrientation).TransformationMatrix();
		std::cout << "Homogenous Transform\n" << T << std::endl;
	}

	std::cout << " Finished" << std::endl;
	return 0;


}

