/*
 * BarrettTech_WAM_FK.cpp
 *
 *  Created on: Nov 13, 2018
 *  Author: Joshua Pickard
 */

#include "kcadl.h"

using namespace kcadl;

int main() {
	// Barrett Technology's WAM 7R manipulator
	ibex::Interval l1 = 0.550;
	ibex::Interval l2 = 0.300;
	ibex::Interval l3 = 0.060;
	ibex::Interval w1 = 0.045;

	// Create twists (screw axes in ee frame)
	Twist twist1(0,0,0,0,0,1);
	Twist twist2(l1+l2+l3,0,0,0,1,0);
	Twist twist3(0,0,0,0,0,1);
	Twist twist4(l2+l3,0,w1,0,1,0);
	Twist twist5(0,0,0,0,0,1);
	Twist twist6(l3,0,0,0,1,0);
	Twist twist7(0,0,0,0,0,1);

	// Create segments
	Segment segmentBase(Joint(Joint::JointType::Base),Frame::Identity());
	Segment segment1(Joint(Joint::JointType::Revolute),Frame::From_twist(twist1));
	Segment segment2(Joint(Joint::JointType::Revolute),Frame::From_twist(twist2));
	Segment segment3(Joint(Joint::JointType::Revolute),Frame::From_twist(twist3));
	Segment segment4(Joint(Joint::JointType::Revolute),Frame::From_twist(twist4));
	Segment segment5(Joint(Joint::JointType::Revolute),Frame::From_twist(twist5));
	Segment segment6(Joint(Joint::JointType::Revolute),Frame::From_twist(twist6));
	Segment segment7(Joint(Joint::JointType::Revolute),Frame::From_twist(twist7));

	// Build kinematic chain
	KinematicChain kinematicChain;
	kinematicChain.verbose = false; // Do not display debugging comments
	kinematicChain.addSegment(segmentBase);
	kinematicChain.addSegment(segment1);
	kinematicChain.addSegment(segment2);
	kinematicChain.addSegment(segment3);
	kinematicChain.addSegment(segment4);
	kinematicChain.addSegment(segment5);
	kinematicChain.addSegment(segment6);
	kinematicChain.addSegment(segment7);

	// Zero transform
	ibex::IntervalMatrix rotationMatrix = ibex::Matrix::eye(3);
	ibex::IntervalVector positionVector(3);
	positionVector[0] = 0;
	positionVector[1] = 0;
	positionVector[2] = l1+l2+l3;
	kinematicChain.zeroTransform(positionVector, rotationMatrix);

	// Find FK solutions - given joint values
	ibex::IntervalVector jointValues(7);
	jointValues[0] = 0;
	jointValues[1] = M_PI/4;
	jointValues[2] = 0;
	jointValues[3] = -M_PI/4;
	jointValues[4] = 0;
	jointValues[5] = -M_PI/2;
	jointValues[6] = 0;
	bool use_filtering = true;
	std::vector<KinematicChain::KinematicsSolution> solutions1 = kinematicChain.solveFK(jointValues, use_filtering);

	// Display FK results
	for (int index=0; index<solutions1.size(); index++){
		ibex::IntervalMatrix T = Frame::Position_and_orientation(solutions1[index].eePosition,solutions1[index].eeOrientation).TransformationMatrix();
		std::cout << "Homogenous Transform\n" << T << std::endl;
	}

	std::cout << " Finished" << std::endl;
	return 0;
}

