/*
 * Human_7dof_upperlimb_FK2.cpp
 *
 *  Created on: Jan 8, 2019
 *  Author: Joshua Pickard
 */

#include "kcadl.h"

using namespace kcadl;

int main() {
	// Human upper limb model
	ibex::Interval lu = 28.74; //upperarm length
	ibex::Interval lf = 27.15; //forearm length
	ibex::Interval lh = 3.76;  //hand length

	// Joint values
	ibex::IntervalVector jointValues(7,0.5 + ibex::Interval(-0.01,0.01));

	// Add frames from DH parameters
	Frame frame01 = Frame::modifiedDH(0,0,0,jointValues[0]);
	Frame frame12 = Frame::modifiedDH(0,M_PI/2,0,jointValues[1]);
	Frame frame23 = Frame::modifiedDH(0,-M_PI/2,lu,jointValues[2]);
	Frame frame34 = Frame::modifiedDH(0,M_PI/2,0,jointValues[3]);
	Frame frame45 = Frame::modifiedDH(0,-M_PI/2,lf,jointValues[4]);
	Frame frame56 = Frame::modifiedDH(0,M_PI/2,0,jointValues[5]);
	Frame frame67 = Frame::modifiedDH(0,-M_PI/2,0,jointValues[6]);
	Frame frame78 = Frame::modifiedDH(0,0,lh,0);

	// Find FK solutions (using constraints)
	bool use_constraints = true;
	Frame frame02 = frame01.FrameMultiply(frame12, use_constraints);
	Frame frame03 = frame02.FrameMultiply(frame23, use_constraints);
	Frame frame04 = frame03.FrameMultiply(frame34, use_constraints);
	Frame frame05 = frame04.FrameMultiply(frame45, use_constraints);
	Frame frame06 = frame05.FrameMultiply(frame56, use_constraints);
	Frame frame07 = frame06.FrameMultiply(frame67, use_constraints);
	Frame frame08 = frame07.FrameMultiply(frame78, use_constraints);

	// Display FK results
	std::cout << "With constraints" << std::endl;
	std::cout << frame08.TransformationMatrix() << std::endl;

	// Find FK solutions (without using constraints)
	use_constraints = false;
	frame02 = frame01.FrameMultiply(frame12, use_constraints);
	frame03 = frame02.FrameMultiply(frame23, use_constraints);
	frame04 = frame03.FrameMultiply(frame34, use_constraints);
	frame05 = frame04.FrameMultiply(frame45, use_constraints);
	frame06 = frame05.FrameMultiply(frame56, use_constraints);
	frame07 = frame06.FrameMultiply(frame67, use_constraints);
	frame08 = frame07.FrameMultiply(frame78, use_constraints);

	// Display FK results
	std::cout << "Without constraints" << std::endl;
	std::cout << frame08.TransformationMatrix() << std::endl;

	std::cout << " Finished" << std::endl;
	return 0;
}








