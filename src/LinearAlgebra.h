/*
 * This software is the property of Auctus, the INRIA research team.
 * Please refer to the Licence file of the repository and Authors
 */

/**
 * @defgroup LinearAlgebra LinearAlgebra
 *
 * @brief Class for interval linear algebra routines.
 *
 * @author Joshua Pickard
 * Contact: joshua.pickard@inria.fr
 *
 */

#ifndef SRC_LINEARALGEBRA_H_
#define SRC_LINEARALGEBRA_H_

#include "ibex.h"

namespace kcadl{
/**
Computes the inverse of an interval matrix exponential using method of:
J. Rohn, INVERSE INTERVAL MATRIX, SIAM J. NUMER. ANAL. Vol. 30, No. 3, pp. 864-870, June 1993
\param A interval matrix
\param Ainv inverse interval matrix
\return success (-1: failed, 1: succeeded)
*/
int Inverse(ibex::IntervalMatrix &A, ibex::IntervalMatrix &Ainv);

/**
Compute the inverse of a matrix
\param Mat matrix to get inverse of
\return matrix inverse
 */
ibex::Matrix Inverse(ibex::Matrix &A);

/**
Check the regularity of an interval matrix using Baumann regularity test
\param intervalMatrix interval matrix to check regularity of
\return regularity (1: regular, -1: not regular)
 */
int Regular(ibex::IntervalMatrix &intervalMatrix);

/**
Compute the determinant of a 3x3 matrix
\param Mat matrix to get determinant of
\return matrix determinant
 */
double Determinant3x3(ibex::Matrix &Mat);

/**
Compute the norm of an interval matrix as:
max_i(sum_j(|A_ij|))
\param A interval matrix to get norm of
\return norm
 */
double Norm(ibex::IntervalMatrix &A);

/**
Compute the norm of an interval vector
\param b interval vector to get norm of
\return norm
 */
double Norm(ibex::IntervalVector &b);

/**
Generate a diagonal matrix
\param V vector to diagonalise
\return diagonal matrix
 */
ibex::Matrix DiagonalMatrix(ibex::Vector &V);

/**
Compute a matrix whose rows are the set of vectors with elements of either 1 or -1
\param nRows number of rows
\return matrix of +/-1 sets
 */
ibex::Matrix PlusMinusOneSet(int nRows);

/**
Compute the set of vertex vectors of an input interval vector
\param impVector input interval vector
\param mpPlusMinusOneSet matrix of +/-1 sets
\param nRows number of rows
\return list of vertex vectors
 */
std::vector<ibex::Vector> VertexVector(ibex::IntervalVector& impVector, ibex::Matrix& mpPlusMinusOneSet, int nRows);

/**
Compute the set of vertex matrices of an input interval matrix
\param impMatrix input interval matrix
\param mpPlusMinusOneSet matrix of +/-1 sets
\param nRows number of rows
\param nCols number of columns
\return list of vertex matrices
 */
std::vector<ibex::Matrix> VertexMatrix(ibex::IntervalMatrix& impMatrix,	ibex::Matrix& mpPlusMinusOneSet, int nRows, int nCols);

/**
Compute an enclosure of the united solution for Ax=b using Krawczyk method with a stopping resolution epsilon.

See Eq. (7.7) in R. Moore, R. Kearfott, and M. Cloud. Introduction to interval analysis. Society for Industrial Mathematics, 2009.

\param A interval matrix A
\param x interval vector x
\param b interval vector b
\param epsilon resolution
\param withPreconditioning flag to enable preconditioning
\return success (0 : uncertain, 1 : solution exists, -1 : solution does not exist)
 */
int krawczyk(ibex::IntervalMatrix &A,
		ibex::IntervalVector &x,
		ibex::IntervalVector &b,
		double epsilon,
		bool withPreconditioning = true);

/**
Compute the cross product of two interval vectors
\param a interval vector a
\param b interval vector b
\return c = a x b
 */
ibex::IntervalVector CrossProduct(ibex::IntervalVector &a, ibex::IntervalVector &b);

/**
Compute an enclosure of the united solution for Ax=b using interval Gauss-Seidel with a stopping resolution epsilon.
\param A interval matrix A
\param x interval vector x
\param b interval vector b
\param epsilon stopping criteria
\param withPreconditioning flag to enable preconditioning
\return success (0 : uncertain, 1 : solution exists, -1 : solution does not exist)
 */
int IntervalGaussSeidel(
		ibex::IntervalMatrix &A,
		ibex::IntervalVector &x,
		ibex::IntervalVector &b,
		double epsilon,
		bool withPreconditioning = true);

}
#endif /* SRC_LINEARALGEBRA_H_ */
