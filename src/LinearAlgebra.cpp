/*
 * This software is the property of Auctus, the INRIA research team.
 * Please refer to the Licence file of the repository and Authors
 */

/**
 * @file LinearAlgebra.cpp
 *
 * @brief Class for interval linear algebra routines.
 *
 * @ingroup LinearAlgebra
 *
 * @author Joshua Pickard
 * Contact: joshua.pickard@inria.fr
 *
 */

#include "LinearAlgebra.h"
#include <cassert>
#include <unistd.h>
#include "ibex.h"

namespace kcadl{
int Inverse(ibex::IntervalMatrix &A, ibex::IntervalMatrix &Ainv) {
	// Compute inverse of an interval matrix
	assert(A.nb_rows()==A.nb_cols());

	// Use Ibex Neumaier inverse routine
	try {
		neumaier_inverse(A, Ainv);
	} catch(...) {
		return -1;
	}
	return 1;
}

ibex::Matrix DiagonalMatrix(ibex::Vector &V){
	ibex::Matrix Diag(V.size(), V.size(), 0.0);
	for (int row=0; row<V.size(); row++)
		Diag[row][row] = V[row];
	return Diag;
}

int Regular(ibex::IntervalMatrix &intervalMatrix){
	// Check regularity of square matrix
	assert(intervalMatrix.nb_rows()==intervalMatrix.nb_cols());

	int nRows = intervalMatrix.nb_rows();
	ibex::Matrix ySet(pow(2, nRows), nRows);
	ibex::Vector z(nRows, 0.0);
	ibex::Vector y(nRows, 1.0);

	int iCount = 0;
	ySet.put(iCount, 0, y, 1);
	while (z != ibex::Vector(nRows, 1.0)) {
		// Find the first 0 in z vector
		int k;
		for (int i = 0; i < nRows; i++)
			if (z[i] == 0) {
				k = i;
				break;
			}
		// update z
		if (k > 0)
			z.put(0, ibex::Vector(k, 0.0));
		z[k] = 1;
		y[k] = -y[k];
		iCount++;
		ySet.put(iCount, 0, y, 1);
	}
	ibex::Matrix zSet = ySet;

	// Baumann regularity test
	int sign = 0;
	bool regular = true;
	for (int y = 0; y < ySet.nb_rows(); y++) {
		for (int z = 0; z < zSet.nb_rows(); z++) {
			// Get matrix from ySet and zSet
			ibex::Matrix subMat(3, 3);
			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++) {
					if (ySet[y][i] * zSet[z][j] == -1)
						subMat[i][j] = intervalMatrix[i][j].ub();
					else
						subMat[i][j] = intervalMatrix[i][j].lb();
				}
			}
			double detersubMat = Determinant3x3(subMat);
			if (sign == 0) { //update sign
				if (detersubMat < 0)
					sign = -1;
				else if (detersubMat > 0)
					sign = 1;
				else {
					return -1;
				}
			} else {
				if (detersubMat < 0 && sign == 1)
					return -1;
				else if (detersubMat > 0 && sign == -1)
					return -1;
			}
		}
	}
	return 1;
}

double Determinant3x3(ibex::Matrix &Mat) {
// Compute determinant of Mat
	double a = Mat[0][0];
	double b = Mat[0][1];
	double c = Mat[0][2];
	double d = Mat[1][0];
	double e = Mat[1][1];
	double f = Mat[1][2];
	double g = Mat[2][0];
	double h = Mat[2][1];
	double i = Mat[2][2];

	double determinant = a * (e * i - h * f) - b * (d * i - f * g)
			+ c * (d * h - e * g);

	return determinant;
}

ibex::Matrix Inverse(ibex::Matrix &A){
	// Check that matrix is square
	assert(A.nb_rows()==A.nb_cols());

	// Use Ibex inverse routine
	return real_inverse(A);
}

ibex::Matrix PlusMinusOneSet(int nRows) {
	ibex::Matrix mpSet(pow(2, nRows), nRows);
	ibex::Vector z(nRows, 0.0);
	ibex::Vector y(nRows, 1.0);

	int iCount = 0;
	mpSet.put(iCount, 0, y, 1);
	while (z != ibex::Vector(nRows, 1.0)) {
		// Find the first 0 in z vector
		int k;
		for (int i = 0; i < nRows; i++)
			if (z[i] == 0) {
				k = i;
				break;
			}
		// update z
		if (k > 0)
			z.put(0, ibex::Vector(k, 0.0));
		z[k] = 1;
		y[k] = -y[k];
		iCount++;
		mpSet.put(iCount, 0, y, 1);
	}
	return mpSet;
}

std::vector<ibex::Vector> VertexVector(ibex::IntervalVector& impVector,
		ibex::Matrix& mpPlusMinusOneSet, int nRows) {
	std::vector < ibex::Vector > lstVertexVectors;

	//generate vertex vector
	for (int i = 0; i < pow(2, nRows); i++) {
		ibex::Vector vpVertexVector(nRows);
		for (int j = 0; j < nRows; j++) {
			vpVertexVector[j] = impVector[j].lb()
					+ (impVector[j].ub() - impVector[j].lb())
							* (1 + mpPlusMinusOneSet[i][j]) / 2;
		}
		lstVertexVectors.push_back(vpVertexVector);
	}
	return lstVertexVectors;
}

std::vector<ibex::Matrix> VertexMatrix(ibex::IntervalMatrix& impMatrix,
		ibex::Matrix& mpPlusMinusOneSet, int nRows, int nCols) {
	std::vector < ibex::Matrix > lstVertexMatrices;

	//generate vertex matrices
	for (int i = 0; i < pow(2, nRows); i++) {
		ibex::Matrix vpVertexMatrix(nRows, nCols);
		for (int k = 0; k < nCols; k++) {
			for (int j = 0; j < nRows; j++) {
				vpVertexMatrix[j][k] = impMatrix[j][k].lb()
						+ (impMatrix[j][k].ub() - impMatrix[j][k].lb())
								* (1 - mpPlusMinusOneSet[i][j]) / 2;
			}
		}
		lstVertexMatrices.push_back(vpVertexMatrix);
	}
	return lstVertexMatrices;
}

ibex::IntervalVector CrossProduct(ibex::IntervalVector &a, ibex::IntervalVector &b){
	ibex::IntervalVector c(3);
	c[0] = a[1]*b[2] - b[1]*a[2];
	c[1] = a[2]*b[0] - a[0]*b[2];
	c[2] = a[0]*b[1] - a[1]*b[0];
	return c;
}


double Norm(ibex::IntervalMatrix &A){
	double norm = 0;
	for (int i=0; i<A.nb_rows(); i++){
		double norm_i = 0;
		for (int j=0; j<A.nb_cols(); j++){
			norm_i += abs(A[i][j]).ub();
		}
		if (norm_i>norm)
			norm = norm_i;
	}
	return norm;
}

double Norm(ibex::IntervalVector &b){
	double norm = 0;
	for (int i=0; i<b.size(); i++){
		double norm_i = abs(b[i]).ub();
		if (norm_i>norm)
			norm = norm_i;
	}
	return norm;
}

int krawczyk(ibex::IntervalMatrix &A, ibex::IntervalVector &x, ibex::IntervalVector &b, double epsilon, bool withPreconditioning){

	// Preconditioner matrix
	ibex::Matrix Y = ibex::Matrix::eye(A.nb_rows());
	if (withPreconditioning || (A.nb_cols() != A.nb_rows())){
		ibex::Matrix Amid = A.mid();
		ibex::Matrix Minv(Amid.nb_cols(), Amid.nb_cols());
		ibex::real_inverse(Amid.transpose()*Amid, Minv); // throw SingularMatrixException
		Y = Minv*Amid.transpose();
	}

	// Loop over krawczyk evaluations
	bool repeat = true;
	bool is_solution = false;
	ibex::IntervalMatrix E = ibex::Matrix::eye(A.nb_cols()) - Y*A;
	ibex::IntervalVector Yb = Y*b;

	// Starting vector x intersected with input value
	if (kcadl::Norm(E) < 1){
		double xk_norm = kcadl::Norm(Yb) / (1-kcadl::Norm(E));
		x &= ibex::IntervalVector(x.size(), ibex::Interval(-xk_norm,xk_norm));
	}

	while(repeat){
		// krawczyk evaluations
		ibex::IntervalVector xk = (Yb + E * x);

		// Empty intersection mean no solution
		if (xk.is_empty()){
			std::cout << "x_i.is_empty" << std::endl;
			return -1;
		} else if (xk.is_subset(x)){
			is_solution = 1;
		} else if (xk.is_superset(x)){
			std::cout << "xk.is_superset(x)" << std::endl;
			return 0;
		}

		// Check resolution
		if (abs(x.diam() - xk.diam()).max() < epsilon)
			repeat = false;

		std::cout << "x  " << x << std::endl;
		std::cout << "xk " << xk << std::endl;

		// Perform the intersection of xk and x
		x &= xk;
	}

	if (is_solution)
		return 1;
	else
		return 0;
}

int IntervalGaussSeidel(ibex::IntervalMatrix &A, ibex::IntervalVector &x, ibex::IntervalVector &b, double epsilon, bool withPreconditioning){
	double max_width = ibex::Interval::POS_REALS.ub();
	bool is_solution = false;

	if (withPreconditioning || (A.nb_cols() != A.nb_rows())){
		// Preconditioner matrix
		ibex::Matrix Amid = A.mid();
		ibex::Matrix Minv(Amid.nb_cols(), Amid.nb_cols());
		ibex::real_inverse(Amid.transpose()*Amid, Minv); // throw SingularMatrixException
		ibex::Matrix P = Minv*Amid.transpose();

		//	 Iterate until stopping criteria satisfied
		while (max_width >= epsilon){
			ibex::IntervalVector x_enc = x;
			// Iterate over all variables xi
			for (int i=0; i<x.size(); i++){
				// Get preconditioning matrix row
				ibex::Vector yi = P.row(i);

				// Evaluate Gauss-Seidel
				ibex::Interval x_i = yi * b;
				for (int j=0; j<i-1; j++) x_i -= yi * A.col(j) * x_enc[j];
				for (int j=i+1; j<x.size(); j++) x_i -= yi * A.col(j) * x[j];

				// Check if vector is empty
				if (x_i.is_empty()){
					std::cout << "x_i.is_empty" << std::endl;
					x.clear();
					return -1;
				}

				// Handle division by 0
				ibex::Interval denom = yi * A.col(i);
				ibex::Interval x_1;
				ibex::Interval x_2;
				ibex::div2(x_i, denom, x_1, x_2);
				if (x_2.is_empty()){
					x_i = x_1;
					x_enc[i] &= x_i;
				} else{
					std::cout << "denom contains zero:" << denom << std::endl;
					x_1 &= x_enc[i];
					x_2 &= x_enc[i];
					x_i = x_1 | x_2;
				}

				// Check if vector is empty
				if (x_enc[i].is_empty()){
					std::cout << "x_enc_.is_empty" << std::endl;
					x.clear();
					return -1;
				}
			}

			// Check if contains unique solution
			if (x_enc.is_interior_subset(x))
				is_solution = true;

			// Get difference in max_width
			max_width = (x.diam() - x_enc.diam()).max();
			x = x_enc;

		}
	} else if (A.nb_cols() == A.nb_rows()) {
		//	 Iterate until stopping criteria satisfied
		while (max_width >= epsilon){
			ibex::IntervalVector x_enc = x;
			// Iterate over all variables xi
			for (int i=0; i<x.size(); i++){
				// Evaluate Gauss-Seidel
				ibex::Interval x_i = b[i];
				for (int j=0; j<i-1; j++) x_i -= A[i][j] * x_enc[j];
				for (int j=i+1; j<x.size(); j++) x_i -= A[i][j] * x[j];

				// Check if vector is empty
				if (x_i.is_empty()){
					std::cout << "x_i.is_empty" << std::endl;
					x.clear();
					return -1;
				}

				// Handle division by 0
				ibex::Interval denom = A[i][i];
				ibex::Interval x_1;
				ibex::Interval x_2;
				ibex::div2(x_i, denom, x_1, x_2);
				if (x_2.is_empty()){
					x_i = x_1;
					x_enc[i] &= x_i;
				} else{
					std::cout << "denom contains zero:" << denom << std::endl;
					x_1 &= x_enc[i];
					x_2 &= x_enc[i];
					x_i = x_1 | x_2;
				}

				// Check if vector is empty
				if (x_enc[i].is_empty()){
					std::cout << "x_enc_.is_empty" << std::endl;
					x.clear();
					return -1;
				}
			}

			// Check if contains unique solution
			if (x_enc.is_interior_subset(x))
				is_solution = true;

			// Get difference in max_width
			max_width = (x.diam() - x_enc.diam()).max();
			x = x_enc;

		}
	}

	if (is_solution)
		return 1;
	else
		return 0;
}
}
