/*
 * This software is the property of Auctus, the INRIA research team.
 * Please refer to the Licence file of the repository and Authors
 */

/**
 * @file KinematicChain.cpp
 *
 * @brief Class for generating imprecise serial kinematic chains from Segment objects.
 *
 * @ingroup KinematicChain
 *
 * @author Joshua Pickard
 * Contact: joshua.pickard@inria.fr
 *
 */

#include "KinematicChain.h"
#include "ExponentialMatrices.h"
#include <math.h>
#include "ibex.h"
#include "Twist.h"
#include <fstream>
#include <iostream>
#include <string>
#include "vibes.h"
#include "Utilities.h"

namespace kcadl{
KinematicChain::KinematicChain() {
	// TODO Auto-generated constructor stub

}

KinematicChain::~KinematicChain() {
	// TODO Auto-generated destructor stub
}

std::vector<KinematicChain::KinematicsSolution> KinematicChain::solveFK(const ibex::IntervalVector &jointValues, bool use_filtering, bool base_reference){
	std::vector<KinematicsSolution> solutionList;

	// Compute the zero transform
	ibex::IntervalMatrix zeroTranform(4,4);
	ibex::IntervalVector lastRow(4,0); lastRow[3] = 1;
	zeroTranform.put(0,0,_zeroOrientation);
	zeroTranform.put(0,3,_zeroPosition, false);
	zeroTranform.put(3,0,lastRow,true);

	// Compute the product of exponentials
	ibex::IntervalMatrix expMatrixProduct = zeroTranform;
	int num_links = segments.size()-1;
	bool compute_exponential = false;

	// Minibex system to enforce orthonormal rotation matrices
	ibex::System system("minibex/Transformation_matrix.mbx");
	ibex::CtcHC4 hc4(system,0.0001);
	ibex::CtcHC4 hc4_2(system,0.001,true);
	ibex::CtcAcid acid(system, hc4_2);
	ibex::CtcCompo compo(hc4,acid);

	if (base_reference == true)
		for (int isegment = num_links; isegment>0; isegment--){
			if (verbose) std::cout << expMatrixProduct << std::endl;
			if (verbose) std::cout << "isegment: " << isegment << std::endl;
			if (verbose) std::cout << segments[isegment].segmentFrame.twist << std::endl;

			// Compute exponential matrix
			ibex::IntervalMatrix expMatrix(4,4);
			if (compute_exponential){
				ibex::IntervalMatrix xi_theta(4,4);
				xi_theta[3][0] = 0;
				xi_theta[3][1] = 0;
				xi_theta[3][2] = 0;
				xi_theta[3][3] = 0;
				xi_theta.put(0,0,segments[isegment].segmentFrame.twist.rotationW);
				xi_theta.put(0,3,segments[isegment].segmentFrame.twist.GetV(), false);
				xi_theta = jointValues[isegment-1] * xi_theta;
//				std::cout << xi_theta << std::endl;
				expMatrix = ExponentialMatrices::GetMatrixExponential(xi_theta);
//				std::cout << "GetMatrixExponential" << std::endl;
//				std::cout << expMatrix << std::endl;
			} else {
				expMatrix = ExponentialMatrices::MatrixExponential(segments[isegment].segmentFrame.twist, jointValues[isegment-1]);
			}

			if (use_filtering){
				// Apply constraints
				ibex::IntervalMatrix sub_expMatrix = expMatrix.submatrix(0,2,0,3);
				ibex::IntervalMatrix sub_expMatrixProduct = expMatrixProduct.submatrix(0,2,0,3);
				ibex::IntervalVector system_box(36);
				system_box.put(0,ibex::flatten(sub_expMatrix));
				system_box.put(12,ibex::flatten(sub_expMatrixProduct));
				compo.contract(system_box);
				compo.contract(system_box);
				ibex::IntervalVector flat_sub_expMatrixProduct = system_box.subvector(24,35);
				sub_expMatrixProduct = ibex::unflatten(flat_sub_expMatrixProduct,3,4);
				expMatrixProduct.put(0,0,sub_expMatrixProduct);
			} else {
				expMatrixProduct = expMatrix * expMatrixProduct;
			}

		}
	else
		for (int isegment = 1; isegment<num_links; isegment++){
			if (verbose) std::cout << expMatrixProduct << std::endl;
			if (verbose) std::cout << "isegment: " << isegment << std::endl;
			if (verbose) std::cout << segments[isegment].segmentFrame.twist << std::endl;

			// Compute exponential matrix
			expMatrixProduct = expMatrixProduct * ExponentialMatrices::MatrixExponential(segments[isegment].segmentFrame.twist, jointValues[isegment-1]);
		}
	if (verbose) std::cout << expMatrixProduct << std::endl;

	// Save FK solution
	KinematicsSolution kinematicsSolution;
	kinematicsSolution.eeOrientation = expMatrixProduct.submatrix(0,2,0,2);
	kinematicsSolution.eePosition = expMatrixProduct.col(3).subvector(0,2);
	kinematicsSolution.jointValues = jointValues;
	solutionList.push_back(kinematicsSolution);

	return solutionList;
}

void KinematicChain::zeroTransform(const ibex::IntervalVector &zeroPosition, const  ibex::IntervalMatrix &zeroOrientation){
	// copy parameters
	_zeroPosition = zeroPosition;
	_zeroOrientation = zeroOrientation;
}

void KinematicChain::addSegment(const Segment &segment){

	if (verbose) std::cout << "Segment #" << segments.size()+1 << std::endl;
	if (verbose) std::cout << "segment.di_Fi " << segment.di_Fi << std::endl;
	if (verbose) std::cout << "segment.pi_Fi " << segment.pi_Fi << std::endl;
	if (verbose) std::cout << "segment.ai_Fi " << segment.ai_Fi << std::endl;
	if (verbose) std::cout << "segment.dip1_Fi " << segment.dip1_Fi << std::endl;
	if (verbose) std::cout << "segment.pim1_Fi " << segment.pim1_Fi << std::endl;

	segments.push_back(segment);
}

std::string KinematicChain::generateMinibex_Basic(const ibex::IntervalVector &positionEE, const  ibex::IntervalMatrix &orientationEE){
	/*--------------------------
	 * Build the system of Porta et al. and solve for the rotation matrices.
	 ------------------------------*/
	// Strings for writing minibex file
	std::string str_constants = "Constants\n";
	std::string str_variables = "\nVariables\n";
	std::string str_constraints = "\nConstraints\n";

	/*--------------------------
	 * CONSTANTS
	 ------------------------------*/
	// d1_F1, d2_F2, ...
	for (int isegment=1; isegment<segments.size(); isegment++){
		ibex::IntervalVector di_Fi = segments[isegment].di_Fi;
		for (int index=0; index<di_Fi.size(); index++){
			if (index==0)      str_constants += "  d"+std::to_string(isegment+1)+"_F"+std::to_string(isegment+1)+"_x";
			else if (index==1) str_constants += "  d"+std::to_string(isegment+1)+"_F"+std::to_string(isegment+1)+"_y";
			else if (index==2) str_constants += "  d"+std::to_string(isegment+1)+"_F"+std::to_string(isegment+1)+"_z";
			str_constants += " in [" + std::to_string(di_Fi[index].lb()) + "," + std::to_string(di_Fi[index].ub()) + "];\n";
		}
	}

	// p1_F1, p2_F2, ...
	for (int isegment=0; isegment<segments.size()-1; isegment++){
		ibex::IntervalVector pi_Fi = segments[isegment].pi_Fi;
		for (int index=0; index<pi_Fi.size(); index++){
			if (index==0)      str_constants += "  p"+std::to_string(isegment+1)+"_F"+std::to_string(isegment+1)+"_x";
			else if (index==1) str_constants += "  p"+std::to_string(isegment+1)+"_F"+std::to_string(isegment+1)+"_y";
			else if (index==2) str_constants += "  p"+std::to_string(isegment+1)+"_F"+std::to_string(isegment+1)+"_z";
			str_constants += " in [" + std::to_string(pi_Fi[index].lb()) + "," + std::to_string(pi_Fi[index].ub()) + "];\n";
		}
	}

//	// d1_F2, d2_F3, ...
//	for (int isegment=1; isegment<segments.size(); isegment++){
//		ibex::IntervalVector dim1_Fi = segments[isegment].dim1_Fi;
//		for (int index=0; index<dim1_Fi.size(); index++){
//			if (index==0)      str_constants += "  d"+std::to_string(isegment)+"_F"+std::to_string(isegment+1)+"_x";
//			else if (index==1) str_constants += "  d"+std::to_string(isegment)+"_F"+std::to_string(isegment+1)+"_y";
//			else if (index==2) str_constants += "  d"+std::to_string(isegment)+"_F"+std::to_string(isegment+1)+"_z";
//			str_constants += " in [" + std::to_string(dim1_Fi[index].lb()) + "," + std::to_string(dim1_Fi[index].ub()) + "];\n";
//		}
//	}

	// d2_F1, d3_F2, ...
	for (int isegment=1; isegment<segments.size(); isegment++){
		ibex::IntervalVector dip1_Fi = segments[isegment].dip1_Fi;
		for (int index=0; index<dip1_Fi.size(); index++){
			if (index==0)      str_constants += "  d"+std::to_string(isegment+1)+"_F"+std::to_string(isegment)+"_x";
			else if (index==1) str_constants += "  d"+std::to_string(isegment+1)+"_F"+std::to_string(isegment)+"_y";
			else if (index==2) str_constants += "  d"+std::to_string(isegment+1)+"_F"+std::to_string(isegment)+"_z";
			str_constants += " in [" + std::to_string(dip1_Fi[index].lb()) + "," + std::to_string(dip1_Fi[index].ub()) + "];\n";
		}
	}

	// p1_F2, p2_F3, ...
	for (int isegment=1; isegment<segments.size(); isegment++){
		ibex::IntervalVector pim1_Fi = segments[isegment].pim1_Fi;
		for (int index=0; index<pim1_Fi.size(); index++){
			if (index==0)      str_constants += "  p"+std::to_string(isegment)+"_F"+std::to_string(isegment+1)+"_x";
			else if (index==1) str_constants += "  p"+std::to_string(isegment)+"_F"+std::to_string(isegment+1)+"_y";
			else if (index==2) str_constants += "  p"+std::to_string(isegment)+"_F"+std::to_string(isegment+1)+"_z";
			str_constants += " in [" + std::to_string(pim1_Fi[index].lb()) + "," + std::to_string(pim1_Fi[index].ub()) + "];\n";
		}
	}

	// R1 = identity
	str_constants += "  R1u_x=1;\n";
	str_constants += "  R1u_y=0;\n";
	str_constants += "  R1u_z=0;\n";
	str_constants += "  R1v_x=0;\n";
	str_constants += "  R1v_y=1;\n";
	str_constants += "  R1v_z=0;\n";
	str_constants += "  R1w_x=0;\n";
	str_constants += "  R1w_y=0;\n";
	str_constants += "  R1w_z=1;\n";

	// r1
	str_constants += "  r1_x=0;\n";
	str_constants += "  r1_y=0;\n";
	str_constants += "  r1_z=0;\n";

	/*--------------------------
	 * VARIABLES
	 ------------------------------*/
	// r2, ...
	for (int isegment=1; isegment<segments.size()-1; isegment++){
		str_variables += "  r" + std::to_string(isegment+1) + "_x in [-oo,oo];\n";
		str_variables += "  r" + std::to_string(isegment+1) + "_y in [-oo,oo];\n";
		str_variables += "  r" + std::to_string(isegment+1) + "_z in [-oo,oo];\n";
	}

	// rE
	int isegment = segments.size();
	str_variables += "  r" + std::to_string(isegment) + "_x in [" + std::to_string(positionEE[0].lb()) + "," + std::to_string(positionEE[0].ub()) +"];\n";
	str_variables += "  r" + std::to_string(isegment) + "_y in [" + std::to_string(positionEE[1].lb()) + "," + std::to_string(positionEE[1].ub()) +"];\n";
	str_variables += "  r" + std::to_string(isegment) + "_z in [" + std::to_string(positionEE[2].lb()) + "," + std::to_string(positionEE[2].ub()) +"];\n";


//	// dE_F1
//	isegment = segments.size();
//	str_variables += "  d" + std::to_string(isegment) + "_F1_x in [-oo,oo];\n";
//	str_variables += "  d" + std::to_string(isegment) + "_F1_y in [-oo,oo];\n";
//	str_variables += "  d" + std::to_string(isegment) + "_F1_z in [-oo,oo];\n";

//	// pE_F1
//	isegment = segments.size();
//	str_variables += "  p" + std::to_string(isegment) + "_F1_x in [-oo,oo];\n";
//	str_variables += "  p" + std::to_string(isegment) + "_F1_y in [-oo,oo];\n";
//	str_variables += "  p" + std::to_string(isegment) + "_F1_z in [-oo,oo];\n";

	// R1, R2, ...
	for (int isegment=1; isegment<segments.size()-1; isegment++){
		for (int index=0; index<3; index++){
			if (index==0)      str_variables += "  R" + std::to_string(isegment+1) + "u_x";
			else if (index==1) str_variables += "  R" + std::to_string(isegment+1) + "u_y";
			else if (index==2) str_variables += "  R" + std::to_string(isegment+1) + "u_z";
			str_variables += + " in [-1,1];\n";
		}
		for (int index=0; index<3; index++){
			if (index==0)      str_variables += "  R" + std::to_string(isegment+1) + "v_x";
			else if (index==1) str_variables += "  R" + std::to_string(isegment+1) + "v_y";
			else if (index==2) str_variables += "  R" + std::to_string(isegment+1) + "v_z";
			str_variables += + " in [-1,1];\n";
		}
		for (int index=0; index<3; index++){
			if (index==0)      str_variables += "  R" + std::to_string(isegment+1) + "w_x";
			else if (index==1) str_variables += "  R" + std::to_string(isegment+1) + "w_y";
			else if (index==2) str_variables += "  R" + std::to_string(isegment+1) + "w_z";
			str_variables += + " in [-1,1];\n";
		}
	}

	// RE
	isegment = segments.size();
	str_variables += "  R" + std::to_string(isegment) + "u_x in [" + std::to_string(orientationEE[0][0].lb()) + "," + std::to_string(orientationEE[0][0].ub()) +"];\n";
	str_variables += "  R" + std::to_string(isegment) + "u_y in [" + std::to_string(orientationEE[1][0].lb()) + "," + std::to_string(orientationEE[1][0].ub()) +"];\n";
	str_variables += "  R" + std::to_string(isegment) + "u_z in [" + std::to_string(orientationEE[2][0].lb()) + "," + std::to_string(orientationEE[2][0].ub()) +"];\n";
	str_variables += "  R" + std::to_string(isegment) + "v_x in [" + std::to_string(orientationEE[0][1].lb()) + "," + std::to_string(orientationEE[0][1].ub()) +"];\n";
	str_variables += "  R" + std::to_string(isegment) + "v_y in [" + std::to_string(orientationEE[1][1].lb()) + "," + std::to_string(orientationEE[1][1].ub()) +"];\n";
	str_variables += "  R" + std::to_string(isegment) + "v_z in [" + std::to_string(orientationEE[2][1].lb()) + "," + std::to_string(orientationEE[2][1].ub()) +"];\n";
	str_variables += "  R" + std::to_string(isegment) + "w_x in [" + std::to_string(orientationEE[0][2].lb()) + "," + std::to_string(orientationEE[0][2].ub()) +"];\n";
	str_variables += "  R" + std::to_string(isegment) + "w_y in [" + std::to_string(orientationEE[1][2].lb()) + "," + std::to_string(orientationEE[1][2].ub()) +"];\n";
	str_variables += "  R" + std::to_string(isegment) + "w_z in [" + std::to_string(orientationEE[2][2].lb()) + "," + std::to_string(orientationEE[2][2].ub()) +"];\n";

	/*--------------------------
	 * CONSTRAINTS
	 ------------------------------*/
	// Rotation matrix orthonormal constraints
	for (int isegment=1; isegment<segments.size(); isegment++){
		// Constraints : rotation matrices (norms of u,v)
		str_constraints += "  1=(R" + std::to_string(isegment+1) + "u_x)^2+(R" + std::to_string(isegment+1) + "u_y)^2+(R" + std::to_string(isegment+1) + "u_z)^2;\n";
		str_constraints += "  1=(R" + std::to_string(isegment+1) + "v_x)^2+(R" + std::to_string(isegment+1) + "v_y)^2+(R" + std::to_string(isegment+1) + "v_z)^2;\n";

		// Constraints : rotation matrices (dot product of u,v)
		str_constraints += "  0=(R" + std::to_string(isegment+1) + "u_x)*(R" + std::to_string(isegment+1) + "v_x)+" +
								"(R" + std::to_string(isegment+1) + "u_y)*(R" + std::to_string(isegment+1) + "v_y)+" +
								"(R" + std::to_string(isegment+1) + "u_z)*(R" + std::to_string(isegment+1) + "v_z)" + ";\n";

		// Constraints : rotation matrices (cross product of u,v)
		str_constraints += "  R" + std::to_string(isegment+1) + "w_x=" +
								"(R" + std::to_string(isegment+1) + "u_y)*(R" + std::to_string(isegment+1) + "v_z)" +
								"-(R" + std::to_string(isegment+1) + "u_z)*(R" + std::to_string(isegment+1) + "v_y)" + ";\n";
		str_constraints += "  R" + std::to_string(isegment+1) + "w_y=" +
								"(R" + std::to_string(isegment+1) + "u_z)*(R" + std::to_string(isegment+1) + "v_x)" +
								"-(R" + std::to_string(isegment+1) + "u_x)*(R" + std::to_string(isegment+1) + "v_z)" + ";\n";
		str_constraints += "  R" + std::to_string(isegment+1) + "w_z=" +
								"(R" + std::to_string(isegment+1) + "u_x)*(R" + std::to_string(isegment+1) + "v_y)" +
								"-(R" + std::to_string(isegment+1) + "u_y)*(R" + std::to_string(isegment+1) + "v_x)" + ";\n";
	}

//	// Axis equation constraints
//	for (int isegment=1; isegment<segments.size(); isegment++){
//		str_constraints += "  R" + std::to_string(isegment+1) + "u_x*d"+std::to_string(isegment)+"_F"+std::to_string(isegment+1)+"_x"+
//						  "+R" + std::to_string(isegment+1) + "v_x*d"+std::to_string(isegment)+"_F"+std::to_string(isegment+1)+"_y"+
//						  "+R" + std::to_string(isegment+1) + "w_x*d"+std::to_string(isegment)+"_F"+std::to_string(isegment+1)+"_z"+
//						  "=R" + std::to_string(isegment) + "u_x*d"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_x"+
//						  "+R" + std::to_string(isegment) + "v_x*d"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_y"+
//						  "+R" + std::to_string(isegment) + "w_x*d"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_z;\n";
//		str_constraints += "  R" + std::to_string(isegment+1) + "u_y*d"+std::to_string(isegment)+"_F"+std::to_string(isegment+1)+"_x"+
//						  "+R" + std::to_string(isegment+1) + "v_y*d"+std::to_string(isegment)+"_F"+std::to_string(isegment+1)+"_y"+
//						  "+R" + std::to_string(isegment+1) + "w_y*d"+std::to_string(isegment)+"_F"+std::to_string(isegment+1)+"_z"+
//						  "=R" + std::to_string(isegment) + "u_y*d"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_x"+
//						  "+R" + std::to_string(isegment) + "v_y*d"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_y"+
//						  "+R" + std::to_string(isegment) + "w_y*d"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_z;\n";
//		str_constraints += "  R" + std::to_string(isegment+1) + "u_z*d"+std::to_string(isegment)+"_F"+std::to_string(isegment+1)+"_x"+
//						  "+R" + std::to_string(isegment+1) + "v_z*d"+std::to_string(isegment)+"_F"+std::to_string(isegment+1)+"_y"+
//						  "+R" + std::to_string(isegment+1) + "w_z*d"+std::to_string(isegment)+"_F"+std::to_string(isegment+1)+"_z"+
//						  "=R" + std::to_string(isegment) + "u_z*d"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_x"+
//						  "+R" + std::to_string(isegment) + "v_z*d"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_y"+
//						  "+R" + std::to_string(isegment) + "w_z*d"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_z;\n";
//	}

	// Axis equation constraints
	for (int isegment=1; isegment<segments.size(); isegment++){
		str_constraints += "  R" + std::to_string(isegment+1) + "u_x*d"+std::to_string(isegment+1)+"_F"+std::to_string(isegment+1)+"_x"+
						  "+R" + std::to_string(isegment+1) + "v_x*d"+std::to_string(isegment+1)+"_F"+std::to_string(isegment+1)+"_y"+
						  "+R" + std::to_string(isegment+1) + "w_x*d"+std::to_string(isegment+1)+"_F"+std::to_string(isegment+1)+"_z"+
						  "=R" + std::to_string(isegment) + "u_x*d"+std::to_string(isegment+1)+"_F"+std::to_string(isegment)+"_x"+
						  "+R" + std::to_string(isegment) + "v_x*d"+std::to_string(isegment+1)+"_F"+std::to_string(isegment)+"_y"+
						  "+R" + std::to_string(isegment) + "w_x*d"+std::to_string(isegment+1)+"_F"+std::to_string(isegment)+"_z;\n";
		str_constraints += "  R" + std::to_string(isegment+1) + "u_y*d"+std::to_string(isegment+1)+"_F"+std::to_string(isegment+1)+"_x"+
						  "+R" + std::to_string(isegment+1) + "v_y*d"+std::to_string(isegment+1)+"_F"+std::to_string(isegment+1)+"_y"+
						  "+R" + std::to_string(isegment+1) + "w_y*d"+std::to_string(isegment+1)+"_F"+std::to_string(isegment+1)+"_z"+
						  "=R" + std::to_string(isegment) + "u_y*d"+std::to_string(isegment+1)+"_F"+std::to_string(isegment)+"_x"+
						  "+R" + std::to_string(isegment) + "v_y*d"+std::to_string(isegment+1)+"_F"+std::to_string(isegment)+"_y"+
						  "+R" + std::to_string(isegment) + "w_y*d"+std::to_string(isegment+1)+"_F"+std::to_string(isegment)+"_z;\n";
		str_constraints += "  R" + std::to_string(isegment+1) + "u_z*d"+std::to_string(isegment+1)+"_F"+std::to_string(isegment+1)+"_x"+
						  "+R" + std::to_string(isegment+1) + "v_z*d"+std::to_string(isegment+1)+"_F"+std::to_string(isegment+1)+"_y"+
						  "+R" + std::to_string(isegment+1) + "w_z*d"+std::to_string(isegment+1)+"_F"+std::to_string(isegment+1)+"_z"+
						  "=R" + std::to_string(isegment) + "u_z*d"+std::to_string(isegment+1)+"_F"+std::to_string(isegment)+"_x"+
						  "+R" + std::to_string(isegment) + "v_z*d"+std::to_string(isegment+1)+"_F"+std::to_string(isegment)+"_y"+
						  "+R" + std::to_string(isegment) + "w_z*d"+std::to_string(isegment+1)+"_F"+std::to_string(isegment)+"_z;\n";

	}

//	// EE segment : Axis equation constraints
//	isegment = segments.size();
//	str_constraints += "  R1u_x*d"+std::to_string(isegment)+"_F1_x+R1v_x*d"+std::to_string(isegment)+"_F1_y+R1w_x*d"+std::to_string(isegment)+"_F1_z=R"+
//					   std::to_string(isegment) + "u_x*d"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_x"+
//					  "+R" + std::to_string(isegment) + "v_x*d"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_y"+
//					  "+R" + std::to_string(isegment) + "w_x*d"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_z;\n";
//	str_constraints += "  R1u_y*d"+std::to_string(isegment)+"_F1_x+R1v_y*d"+std::to_string(isegment)+"_F1_y+R1w_y*d"+std::to_string(isegment)+"_F1_z=R"+
//					   std::to_string(isegment) + "u_y*d"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_x"+
//					  "+R" + std::to_string(isegment) + "v_y*d"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_y"+
//					  "+R" + std::to_string(isegment) + "w_y*d"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_z;\n";
//	str_constraints += "  R1u_z*d"+std::to_string(isegment)+"_F1_x+R1v_z*d"+std::to_string(isegment)+"_F1_y+R1w_z*d"+std::to_string(isegment)+"_F1_z=R"+
//			           std::to_string(isegment) + "u_z*d"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_x"+
//					  "+R" + std::to_string(isegment) + "v_z*d"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_y"+
//					  "+R" + std::to_string(isegment) + "w_z*d"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_z;\n";

	// Position constraints
	for (int isegment=1; isegment<segments.size(); isegment++){
		str_constraints += "  r" + std::to_string(isegment+1) + "_x" +
				 	 	  "+R" + std::to_string(isegment+1) + "u_x*p"+std::to_string(isegment)+"_F"+std::to_string(isegment+1)+"_x"+
						  "+R" + std::to_string(isegment+1) + "v_x*p"+std::to_string(isegment)+"_F"+std::to_string(isegment+1)+"_y"+
						  "+R" + std::to_string(isegment+1) + "w_x*p"+std::to_string(isegment)+"_F"+std::to_string(isegment+1)+"_z"+
						  "=r" + std::to_string(isegment) + "_x" +
						  "+R" + std::to_string(isegment) + "u_x*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_x"+
						  "+R" + std::to_string(isegment) + "v_x*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_y"+
						  "+R" + std::to_string(isegment) + "w_x*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_z;\n";
		str_constraints += "  r" + std::to_string(isegment+1) + "_y" +
		 	 	  	  	  "+R" + std::to_string(isegment+1) + "u_y*p"+std::to_string(isegment)+"_F"+std::to_string(isegment+1)+"_x"+
						  "+R" + std::to_string(isegment+1) + "v_y*p"+std::to_string(isegment)+"_F"+std::to_string(isegment+1)+"_y"+
						  "+R" + std::to_string(isegment+1) + "w_y*p"+std::to_string(isegment)+"_F"+std::to_string(isegment+1)+"_z"+
						  "=r" + std::to_string(isegment) + "_y" +
						  "+R" + std::to_string(isegment) + "u_y*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_x"+
						  "+R" + std::to_string(isegment) + "v_y*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_y"+
						  "+R" + std::to_string(isegment) + "w_y*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_z;\n";
		str_constraints += "  r" + std::to_string(isegment+1) + "_z" +
		 	 	  	  	  "+R" + std::to_string(isegment+1) + "u_z*p"+std::to_string(isegment)+"_F"+std::to_string(isegment+1)+"_x"+
						  "+R" + std::to_string(isegment+1) + "v_z*p"+std::to_string(isegment)+"_F"+std::to_string(isegment+1)+"_y"+
						  "+R" + std::to_string(isegment+1) + "w_z*p"+std::to_string(isegment)+"_F"+std::to_string(isegment+1)+"_z"+
						  "=r" + std::to_string(isegment) + "_z" +
						  "+R" + std::to_string(isegment) + "u_z*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_x"+
						  "+R" + std::to_string(isegment) + "v_z*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_y"+
						  "+R" + std::to_string(isegment) + "w_z*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_z;\n";
	}

//	// EE segment : Position constraints
//	isegment = segments.size();
//	str_constraints += "  r1_x+R1u_x*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_x"+
//					  "+R1v_x*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_y"+
//					  "+R1w_x*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_z"+
//					  "=r" + std::to_string(isegment) + "_x" +
//					  "+R" + std::to_string(isegment) + "u_x*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_x"+
//					  "+R" + std::to_string(isegment) + "v_x*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_y"+
//					  "+R" + std::to_string(isegment) + "w_x*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_z;\n";
//	str_constraints += "  r1_y+R1u_y*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_x"+
//					  "+R1v_y*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_y"+
//					  "+R1w_y*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_z"+
//					  "=r" + std::to_string(isegment) + "_y" +
//					  "+R" + std::to_string(isegment) + "u_y*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_x"+
//					  "+R" + std::to_string(isegment) + "v_y*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_y"+
//					  "+R" + std::to_string(isegment) + "w_y*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_z;\n";
//	str_constraints += "  r1_z+R1u_z*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_x"+
//					  "+R1v_z*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_y"+
//					  "+R1w_z*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_z"+
//					  "=r" + std::to_string(isegment) + "_z" +
//					  "+R" + std::to_string(isegment) + "u_z*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_x"+
//					  "+R" + std::to_string(isegment) + "v_z*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_y"+
//					  "+R" + std::to_string(isegment) + "w_z*p"+std::to_string(isegment)+"_F"+std::to_string(isegment)+"_z;\n";

	// Write minibex file
	std::string str = "minibex/IK_minibex.txt";
	std::ofstream outfile (str);
	outfile << str_constants << std::endl;
	outfile << str_variables << std::endl;
	outfile << str_constraints << std::endl;
	outfile << "end" << std::endl;
	return str;
}

std::vector<KinematicChain::KinematicsSolution> KinematicChain::solveIK(const ibex::IntervalVector &positionEE, const  ibex::IntervalMatrix &orientationEE){
	std::vector<KinematicsSolution> solutionList;

	// Generate Minibex
	std::string file = generateMinibex_Basic(positionEE, orientationEE);

	// Create the solver
	const char *cstr = file.c_str();
	ibex::System system(cstr);
	if (verbose) std::cout << system << std::endl;

	/* ============================ building contractors ========================= */
	ibex::CtcHC4 hc4(system,0.01);
	ibex::CtcHC4 hc4_2(system,0.1,true);
	ibex::CtcAcid acid(system, hc4_2);
	ibex::CtcCompo compo(hc4,acid);

	/* =========================================================================== */

	/* Create a smear-function bisection heuristic. */
	ibex::SmearSumRelative bisector(system, 1e-02);

	/* Create a "stack of boxes" (CellStack) (depth-first search). */
	ibex::CellStack buff;

	/* Vector precisions required on variables */
	ibex::Vector prec(6, 1e-02);

	/* Create a solver with the previous objects */
	ibex::Solver s(system, compo, bisector, buff, prec, prec);

	// Run the solver
	s.solve(system.box);
	const ibex::CovList& cov = s.get_data();

	// Display the solutions
	if (verbose) std::cout << "Solutions" << std::endl;
	if (verbose) std::cout << cov.size() << std::endl;
	std::vector<ibex::IntervalVector> solverSolutions;
	for (size_t i=0; i<cov.size(); i++) {
		if (verbose) std::cout << "box n°" << i << " = " << cov[i] << std::endl;
		solverSolutions.push_back(cov[i]);
	}
	// Compile the transformation matrices for each segment
	solutionList = KinematicChain::computeJointAngles(solverSolutions);

	std::cout << "Done" << std::endl;
	return solutionList;
}

std::vector<KinematicChain::KinematicsSolution> KinematicChain::computeJointAngles(std::vector<ibex::IntervalVector> &solverSolutions){

	// Vector to store joint angles for each solution
	std::vector<KinematicsSolution> kinematicsSolutions;

	// Iterate over all solutions to determine joint angles
	int solutionNum = 0;
	while(solverSolutions.size()>0){
		// Structure to save solutions
		KinematicsSolution kinematicsSolution;

		if (verbose) std::cout << "#############################" << std::endl;
		if (verbose) std::cout << "Solution: " << solutionNum << std::endl;
		solutionNum++;
		ibex::IntervalVector box = solverSolutions.back();
		solverSolutions.pop_back();

		std::deque<Frame> listOfFramesWRTBase;
	    ibex::IntervalVector framePosition(3);
	    ibex::IntervalMatrix frameOrientation(3,3);

	    // Save joint positions wrt base frame
	    std::vector<ibex::IntervalVector> framePositions;
	    framePositions.push_back(ibex::IntervalVector(3,0));

	    int num_links = segments.size()-1;
	    int length_of_r = 3;
	    int length_of_R = 9;
		for(int isegment=0; isegment<num_links; isegment++){
			framePosition[0] = box[isegment*length_of_r];
			framePosition[1] = box[isegment*length_of_r+1];
			framePosition[2] = box[isegment*length_of_r+2];

			frameOrientation[0][0] = box[num_links*length_of_r+isegment*length_of_R];
			frameOrientation[1][0] = box[num_links*length_of_r+isegment*length_of_R+1];
			frameOrientation[2][0] = box[num_links*length_of_r+isegment*length_of_R+2];
			frameOrientation[0][1] = box[num_links*length_of_r+isegment*length_of_R+3];
			frameOrientation[1][1] = box[num_links*length_of_r+isegment*length_of_R+4];
			frameOrientation[2][1] = box[num_links*length_of_r+isegment*length_of_R+5];
			frameOrientation[0][2] = box[num_links*length_of_r+isegment*length_of_R+6];
			frameOrientation[1][2] = box[num_links*length_of_r+isegment*length_of_R+7];
			frameOrientation[2][2] = box[num_links*length_of_r+isegment*length_of_R+8];

			// Save the frame to the list -- associated twists are also computed here
			Frame frame = Frame::Position_and_orientation(framePosition, frameOrientation, 1, isegment+2);
			// Update frame DH parameters from original frame
			frame.DH_a = segments[isegment+1].segmentFrame.DH_a;
			frame.DH_alpha = segments[isegment+1].segmentFrame.DH_alpha;
			frame.DH_d = segments[isegment+1].segmentFrame.DH_d;
			frame.DH_theta = segments[isegment+1].segmentFrame.DH_theta;
			if (verbose) std::cout << frame << std::endl;
			listOfFramesWRTBase.push_back(frame);

			// Save joint positions wrt base frame
			framePositions.push_back(frame.position);
		}

		// Populate kinematicsSolutions
		kinematicsSolution.eePosition = listOfFramesWRTBase.back().position;
		kinematicsSolution.eeOrientation = listOfFramesWRTBase.back().orientation;
		kinematicsSolution.framePositions = framePositions;

		// Convert frames so that reference frame is the previous frame;
		if (verbose) std::cout << "Change Reference Frame: " << std::endl;
		if (verbose) std::cout << "----------------------------" << std::endl;
		std::deque<Frame> listOfFramesWRTPrevious;
		// Add first frame - it is already wrt the previous frame
		listOfFramesWRTPrevious.push_back(listOfFramesWRTBase.front());
		if (verbose) std::cout << listOfFramesWRTBase.front() << std::endl;
		while(listOfFramesWRTBase.size()>1){
			Frame frameWRTBasePrevious = listOfFramesWRTBase.front();
			listOfFramesWRTBase.pop_front();
			Frame frameWRTBaseCurrent = listOfFramesWRTBase.front();

			// Compute frame as i_T_i+1 = Transpose(0_T_i) * 0_T_i+1
			Frame frameWRTBasePreviousTransposed = frameWRTBasePrevious.Transpose();
			Frame frameWRTPrevious = frameWRTBasePreviousTransposed.FrameMultiply(frameWRTBaseCurrent);
			if (verbose) std::cout << frameWRTPrevious << std::endl;
			listOfFramesWRTPrevious.push_back(frameWRTPrevious);
		}

		// Compute joints angles using Paden-Kahan subproblem;
		if (verbose) std::cout << "Compute Joint Angles: " << std::endl;
		if (verbose) std::cout << "----------------------------" << std::endl;
		ibex::IntervalVector jointValues(listOfFramesWRTPrevious.size());
		int jointNum = 0;
		while(listOfFramesWRTPrevious.size()>0){
			Frame frameWRTPrevious = listOfFramesWRTPrevious.front();
			listOfFramesWRTPrevious.pop_front();

			// Compute expontential matrix
			ibex::IntervalMatrix exponentialMatrix = frameWRTPrevious.TransformationMatrix() * frameWRTPrevious.InverseZeroTransform();

			// Paden-Kahan subproblem 1:
			ibex::IntervalVector pointR_3(3, 0); // Point on twist axis
			pointR_3[2] = -1;
			ibex::IntervalVector pointP_3(3, 0); // Point not on twist axis
			pointP_3[1] = 1;
			ibex::IntervalVector pointP_4(4,1); // Used for multiplication with exponentialMatrix
			pointP_4[0] = pointP_3[0];
			pointP_4[1] = pointP_3[1];
			pointP_4[2] = pointP_3[2];
			ibex::IntervalVector pointQ_4 = exponentialMatrix * pointP_4; // Point not on twist axis
			ibex::IntervalVector pointQ_3 = pointQ_4.subvector(0,2);
			ibex::IntervalVector vectorU = pointP_3 - pointR_3;
			ibex::IntervalVector vectorV = pointQ_3 - pointR_3;
			ibex::IntervalVector vectorW = frameWRTPrevious.twist.GetW(); // Unit vector along twist axis
			ibex::IntervalMatrix matrixW(3,1);
			matrixW[0][0] = vectorW[0];
			matrixW[1][0] = vectorW[1];
			matrixW[2][0] = vectorW[2];
			ibex::IntervalVector vectorU2 = vectorU - matrixW * matrixW.transpose() * vectorU; // Projected onto XY plane
			ibex::IntervalVector vectorV2 = vectorV - matrixW * matrixW.transpose() * vectorW; // Projected onto XY plane
			ibex::Interval tan2y = vectorW*(ibex::cross(vectorU2,vectorV2));
			ibex::Interval tan2x = vectorU2*vectorV2;
			ibex::Interval theta = atan2(tan2y, tan2x);

			// Save joint angle to vector
			jointValues[jointNum] = theta;

			// Increment joint angle counter
			jointNum++;
		}

		// Populate kinematicsSolutions
		kinematicsSolution.jointValues = jointValues;
		kinematicsSolutions.push_back(kinematicsSolution);

	}

	return kinematicsSolutions;

}


//std::vector<ibex::IntervalVector> KinematicChain::reachableWorkspace(ibex::IntervalVector &searchBox, double resolution){
//	std::vector<ibex::IntervalVector> reachableList;
//	std::deque<ibex::IntervalVector> unclassifiedList;
//	unclassifiedList.push_back(searchBox);
//
//	// Orientation
//	double _orientation_pose[9] = {1,0,0,0,1,0,0,0,1}; // seems to remove EE orientation from problem
//	const ibex::IntervalMatrix orientation_pose = ibex::IntervalMatrix(ibex::Matrix(3,3,_orientation_pose));
//
//	// Solve reachable portions of searchBox
//	while (unclassifiedList.size()>0){
//		// Get current search box
//		ibex::IntervalVector currentSearchBox=unclassifiedList.front();
//		unclassifiedList.pop_front();
//
//		// Generate Minibex
//		std::string file = generateMinibex(currentSearchBox, orientation_pose);
//		const char *cstr = file.c_str();
//
//		// Create the solver
//		ibex::System system(cstr);
//
//		// Get maximum box of unknowns
//		ibex::IntervalVector box = system.box;
//		ibex::IntervalVector constraintBox = box;
//
//		// Generate Contractor
//		ibex::CtcHC4 hc4(system,0.01);
//		ibex::CtcHC4 hc4_2(system,0.1,true);
//		ibex::CtcAcid acid(system, hc4_2);
//		ibex::CtcCompo compo (hc4,acid);
//
//		// Create a bisection heuristic.
//		ibex::LargestFirst bisector(resolution,0.5);
//
//		// Run the contractor
//		std::cout << box << std::endl;
//		compo.contract(box);
//		compo.contract(box);
//		compo.contract(box);
//		std::cout << box << std::endl;
//
//		// Check for solutions
//		vibes::drawBox(currentSearchBox[0].lb(), currentSearchBox[0].ub(), currentSearchBox[1].lb(), currentSearchBox[1].ub(),"k");
//		if (!box.is_empty()){
//			// Check reachability conditions -- TODO
//			bool reachable = false;
//			if (reachable){
//				std::cout << "REACHABLE\t" << currentSearchBox << std::endl;
//				vibes::drawBox(currentSearchBox[0].lb(), currentSearchBox[0].ub(), currentSearchBox[1].lb(), currentSearchBox[1].ub(),"b[b]");
//				reachableList.push_back(box);
//			}
//			else if (box.diam().max()>resolution && currentSearchBox.is_bisectable()){
//
//				std::pair<ibex::IntervalVector,ibex::IntervalVector> bisectpair = bisector.bisect(currentSearchBox);
//				unclassifiedList.push_back(bisectpair.first);
//				unclassifiedList.push_back(bisectpair.second);
//			} else {
//				std::cout << "BOUNDARY\t" << currentSearchBox << std::endl;
//				vibes::drawBox(currentSearchBox[0].lb(), currentSearchBox[0].ub(), currentSearchBox[1].lb(), currentSearchBox[1].ub(),"y[y]");
//				reachableList.push_back(box);
//			}
//		} else {
//			std::cout << "unreachable\t" << currentSearchBox << std::endl;
//			vibes::drawBox(currentSearchBox[0].lb(), currentSearchBox[0].ub(), currentSearchBox[1].lb(), currentSearchBox[1].ub(),"r[r]");
//		}
//
//	}
//
//	return reachableList;
//}
}
