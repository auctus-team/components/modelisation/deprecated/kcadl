/*
 * This software is the property of Auctus, the INRIA research team.
 * Please refer to the Licence file of the repository and Authors
 */

/**
 * @defgroup Frame Frame
 *
 * @brief Class for coordinate frames.
 *
 * @author Joshua Pickard
 * Contact: joshua.pickard@inria.fr
 *
 */

#ifndef SRC_FRAME_H_
#define SRC_FRAME_H_

#include "ibex.h"
#include "Twist.h"
#include "Utilities.h"

namespace kcadl{
class Frame {
	friend class Segment;
	friend class KinematicChain;

public:
	/**
	Frame contructor
	 */
	Frame() {}
	virtual ~Frame();

	ibex::IntervalVector position = ibex::IntervalVector(3); /**< frame position */
	ibex::IntervalMatrix orientation = ibex::IntervalMatrix(3,3); /**< frame orientation */
	Twist twist; /**< twist */
	int referenceFrame = 0; /**< reference frame ID */
	int describedFrame = 0; /**< current frame ID */
	ibex::Interval DH_a = 0; /**< DH parameter: a */
	ibex::Interval DH_alpha = 0; /**< DH parameter: alpha */
	ibex::Interval DH_d = 0; /**< DH parameter: d */
	ibex::Interval DH_theta = 0; /**< DH parameter: theta */
	ibex::Interval DH_type = -1; /**< DH type: -1 - none, 0 - classical, 1 - modified */

	/**
	Update DH_d value (prismatic actuator)
	 */
	void Update_DH_d(ibex::Interval new_DH_d);

	/**
	Update DH_theta value (revolute actuator)
	 */
	void Update_DH_theta(ibex::Interval new_DH_theta);

	/**
	Get frame tranpose
	\return tranposed frame
	 */
	Frame Transpose();

	/**
	Get transformation matrix
	\return transformation matrix
	 */
	ibex::IntervalMatrix TransformationMatrix();

	/**
	Get transformation matrix
	\param framePosition interval vector describing the frame position
	\param frameOrientation interval matrix describing the frame orientation
	\return transformation matrix
	 */
	ibex::IntervalMatrix TransformationMatrix(ibex::IntervalVector &framePosition, ibex::IntervalMatrix &frameOrientation);

	/**
	Display frame information
	 */
	void Print(){
		std::cout << "orientation\t" << orientation << "\n"
				<< "position\t" << position << "\n"
				<< "twist\t" << twist << "\n"
				<< "DH_a\t" << DH_a << "\n"
				<< "DH_alpha\t" << DH_alpha << "\n"
				<< "DH_d\t" << DH_d << "\n"
				<< "DH_theta\t" << DH_theta << "\n"
				<< "referenceFrame\t" << referenceFrame << "\n"
				<< "describedFrame\t" << describedFrame << std::endl;;
	}

	/**
	Multiply frames (parameter multiplied on right)
	\param frame frame to be multiplied
	\param use_constraints use constraints to improve evaluation
	\return product frame
	 */
	Frame FrameMultiply(Frame &frame, bool use_constraints=false);

	/**
	Create frame from position and orientation
	\param framePosition interval vector describing the frame position
	\param frameOrientation interval matrix describing the frame orientation
	\return frame
	 */
	static Frame Position_and_orientation(const ibex::IntervalVector &framePosition, const ibex::IntervalMatrix &frameOrientation);

	/**
	Create create from position and orientation with references
	\param framePosition interval vector describing the frame position
	\param frameOrientation interval matrix describing the frame orientation
	\param referenceFrame reference frame
	\param describedFrame current frame
	\return frame
	 */
	static Frame Position_and_orientation(const ibex::IntervalVector &framePosition, const ibex::IntervalMatrix &frameOrientation, int referenceFrame,  int describedFrame);

	/**
	Create identity frame
	\return frame
	 */
	static Frame Identity();

	/**
	Create frame from classical DH parameters
	\param a
	\param alpha
	\param d
	\param theta
	\return frame
	 */
	static Frame DH(ibex::Interval a, ibex::Interval alpha, ibex::Interval d, ibex::Interval theta);

	/**
	Create frame from modified DH parameters
	\param a
	\param alpha
	\param d
	\param theta
	\return frame
	 */
	static Frame modifiedDH(ibex::Interval a, ibex::Interval alpha, ibex::Interval d, ibex::Interval theta);

	/**
	 * \brief Create frame object from a twist.
	 */
	static Frame From_twist(const Twist &twist);

	/**
	 * \brief Create frame object from a twist and frame references.
	 */
	static Frame From_twist(const Twist &twist, int referenceFrame,  int describedFrame);

	/**
	Display frame transformation matrix
	 */
	friend std::ostream& operator<<(std::ostream &os, Frame &frame){
		ibex::IntervalMatrix transform = frame.TransformationMatrix();
		return os << transform << "\n";
	}

private:
	/**
	Get zero transform
	\return zero transform
	 */
	ibex::IntervalMatrix ZeroTransform();

	/**
	Get inverse zero transform
	\return inverse zero transform
	 */
	ibex::IntervalMatrix InverseZeroTransform();

	/**
	Updates values when DH values modified
	 */
	void Update_DH();

};

/** \ingroup arithmetic */
/*@{*/

/**
 * \brief Return Fij*Fjk.
 */
Frame operator*(Frame &Fij, Frame &Fjk);

/*@}*/

} // namespace kcadl

/*================================== inline implementations ========================================*/
namespace kcadl{

inline Frame operator*(Frame &Fij, Frame &Fjk) {
	Frame Fik = Fij.FrameMultiply(Fjk);
	return Fik;
}

inline Frame Frame::Position_and_orientation(const ibex::IntervalVector &framePosition, const ibex::IntervalMatrix &frameOrientation) {
	Frame newFrame;

	// copy parameters
	newFrame.position = framePosition;
	newFrame.orientation = frameOrientation;

	return newFrame;
}

inline Frame Frame::Position_and_orientation(const ibex::IntervalVector &framePosition, const ibex::IntervalMatrix &frameOrientation, int referenceFrame,  int describedFrame){
	Frame newFrame;

	// copy parameters
	newFrame.position = framePosition;
	newFrame.orientation = frameOrientation;
	newFrame.referenceFrame = referenceFrame;
	newFrame.describedFrame = describedFrame;

	// Get associated twist
	ibex::IntervalVector W = newFrame.orientation.col(2); // twist axis
	ibex::IntervalVector V = ibex::cross(-W, newFrame.position); // position of twist axis
	newFrame.twist = Twist(V, W);

	return newFrame;
}

inline Frame Frame::Identity() {
	Frame newFrame;

	// copy parameters
	newFrame.position = ibex::IntervalVector(3,0);
	newFrame.orientation = ibex::IntervalMatrix(ibex::Matrix::eye(3));

	return newFrame;
}

inline Frame Frame::DH(ibex::Interval a, ibex::Interval alpha, ibex::Interval d, ibex::Interval theta){
	// returns Denavit-Hartenberg parameters (Non-Modified DH)
	ibex::Interval ct,st,ca,sa;
	ct = cos(theta);
	st = sin(theta);
	sa = sin(alpha);
	ca = cos(alpha);

	ibex::IntervalMatrix frameOrientation(3,3);
	frameOrientation[0][0] = ct;
	frameOrientation[0][1] = -st*ca;
	frameOrientation[0][2] = st*sa;
	frameOrientation[1][0] = st;
	frameOrientation[1][1] = ct*ca;
	frameOrientation[1][2] = -ct*sa;
	frameOrientation[2][0] = 0;
	frameOrientation[2][1] = sa;
	frameOrientation[2][2] = ca;

	ibex::IntervalVector framePosition(3);
	framePosition[0] = a*ct;
	framePosition[1] = a*st;
	framePosition[2] = d;

	Frame newFrame;
	newFrame.position = framePosition;
	newFrame.orientation = frameOrientation;
	newFrame.DH_a = a;
	newFrame.DH_alpha = alpha;
	newFrame.DH_d = d;
	newFrame.DH_theta = theta;
	newFrame.DH_type = 0;  // classical parameters

	return newFrame;
}

inline Frame Frame::modifiedDH(ibex::Interval a, ibex::Interval alpha, ibex::Interval d, ibex::Interval theta){
	// returns Denavit-Hartenberg parameters (Modified DH)
	ibex::Interval ct,st,ca,sa;
	ct = cos(theta);
	st = sin(theta);
	sa = sin(alpha);
	ca = cos(alpha);

	ibex::IntervalMatrix frameOrientation(3,3);
	frameOrientation[0][0] = ct;
	frameOrientation[0][1] = -st;
	frameOrientation[0][2] = 0;
	frameOrientation[1][0] = st*ca;
	frameOrientation[1][1] = ct*ca;
	frameOrientation[1][2] = -sa;
	frameOrientation[2][0] = st*sa;
	frameOrientation[2][1] = ct*sa;
	frameOrientation[2][2] = ca;

	ibex::IntervalVector framePosition(3);
	framePosition[0] = a;
	framePosition[1] = -sa*d;
	framePosition[2] = ca*d;

	Frame newFrame;
	newFrame.position = framePosition;
	newFrame.orientation = frameOrientation;
	newFrame.DH_a = a;
	newFrame.DH_alpha = alpha;
	newFrame.DH_d = d;
	newFrame.DH_theta = theta;
	newFrame.DH_type = 1;  // modified parameters

	return newFrame;
}

inline Frame Frame::From_twist(const Twist &twist, int referenceFrame,  int describedFrame){
	Frame newFrame;

	// copy parameters
	newFrame.twist = twist;
	newFrame.referenceFrame = referenceFrame;
	newFrame.describedFrame = describedFrame;

	return newFrame;
}

inline Frame Frame::From_twist(const Twist &twist){
	Frame newFrame;

	// copy parameters
	newFrame.twist = twist;

	return newFrame;
}
} // namespace kcadl
#endif /* SRC_FRAME_H_ */
