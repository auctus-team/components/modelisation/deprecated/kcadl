/*
 * This software is the property of Auctus, the INRIA research team.
 * Please refer to the Licence file of the repository and Authors
 */

/**
 * @defgroup Segment Segment
 *
 * @brief Class for generating segments for building imprecise serial kinematic chains.
 *
 * @author Joshua Pickard
 * Contact: joshua.pickard@inria.fr
 *
 */

#ifndef SRC_SEGMENT_H_
#define SRC_SEGMENT_H_

#include "Joint.h"
#include "Frame.h"
#include "ibex.h"

namespace kcadl{
class Segment {
	friend class KinematicChain;

public:
	/**
	Segment constructor
	\param joint joint
	\param frame frame
	 */
	Segment(const Joint &joint, const Frame &frame);

	/**
	Segment destructor
	 */
	virtual ~Segment();

private:
	Joint segmentJoint; /**< Joint associated with segment*/
	Frame segmentFrame; /**< Frame associated with segment*/

	// Segment parameters (for Porta IK solution)
	double _di_Fi[3] = { 0.0, 0.0, 1.0 };
	double _pi_Fi[3] = { 0.0, 0.0, -1.0 };
	ibex::IntervalVector di_Fi = ibex::IntervalVector(ibex::Vector(3, _di_Fi)); /**< joint axis i in frame i */
	ibex::IntervalVector pi_Fi = ibex::IntervalVector(ibex::Vector(3, _pi_Fi)); /**< point displayed along -z */
	ibex::IntervalVector ai_Fi = ibex::IntervalVector(3); /**< ai_Fi */
	ibex::IntervalVector di_Fip1 = ibex::IntervalVector(3); /**< di_Fip1 */
	ibex::IntervalVector pim1_Fi = ibex::IntervalVector(3); /**< pim1_Fi */
	ibex::IntervalVector dim1_Fi = ibex::IntervalVector(3); /**< dim1_Fi */
	ibex::IntervalVector dip1_Fi = ibex::IntervalVector(3); /**< dip1_Fi */

	friend std::ostream& operator<<(std::ostream &os, Segment &segment){
		return os << "di_Fi\t" << segment.di_Fi << "\n"
				<< "segment.di_Fi " << segment.di_Fi << "\n"
				<< "segment.pi_Fi " << segment.pi_Fi << "\n"
				<< "segment.ai_Fi " << segment.ai_Fi << "\n"
				<< "segment.di_Fip1 " << segment.di_Fip1 << "\n"
				<< "segment.pi_Fip1 " << segment.pim1_Fi << "\n";
	}
};
}
#endif /* SRC_SEGMENT_H_ */
