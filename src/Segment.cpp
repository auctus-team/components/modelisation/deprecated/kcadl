/*
 * This software is the property of Auctus, the INRIA research team.
 * Please refer to the Licence file of the repository and Authors
 */

/**
 * @file Segment.cpp
 *
 * @brief Class for generating segments for building imprecise serial kinematic chains.
 *
 * @ingroup Segment
 *
 * @author Joshua Pickard
 * Contact: joshua.pickard@inria.fr
 *
 */

#include "Segment.h"
#include "Joint.h"
#include "Frame.h"

namespace kcadl{
Segment::Segment(const Joint &joint, const Frame &frame) {
	// copy parameters
	segmentJoint = joint;
	segmentFrame = frame;

	// evaluate parameters from DH parameters
	ai_Fi[0] = frame.DH_a;
	ai_Fi[1] = 0;
	ai_Fi[2] = -1;

	di_Fi[0] = 0;
	di_Fi[1] = 0;
	di_Fi[2] = frame.DH_d+1;

	di_Fip1[0] = 0;
	di_Fip1[1] = (frame.DH_d+1)*sin(-frame.DH_alpha);
	di_Fip1[2] = (frame.DH_d+1)*cos(-frame.DH_alpha);

	dip1_Fi[0] = 0;
	dip1_Fi[1] = -sin(frame.DH_alpha);
	dip1_Fi[2] = cos(frame.DH_alpha);

	// Planar only
	pim1_Fi[0] = -frame.DH_a;
	pim1_Fi[1] = -sin(frame.DH_alpha);
	pim1_Fi[2] = -cos(frame.DH_alpha);

}

Segment::~Segment() {
	// TODO Auto-generated destructor stub
}
}

