/*
 * This software is the property of Auctus, the INRIA research team.
 * Please refer to the Licence file of the repository and Authors
 */

/**
 * @defgroup ChainGeometry ChainGeometry
 *
 * @brief Class for generating imprecise serial kinematic chains from SegmentGeometry objects.
 *
 * @author Joshua Pickard
 * Contact: joshua.pickard@inria.fr
 *
 */

#ifndef SRC_CHAINGEOMETRY_H_
#define SRC_CHAINGEOMETRY_H_

#include <vector>
#include "SegmentGeometry.h"
#include <CGAL/Homogeneous.h>
#include <CGAL/Polytope_distance_d.h>
#include <CGAL/Polytope_distance_d_traits_3.h>
#include "ibex.h"

#ifdef CGAL_USE_GMP
#include <CGAL/Gmpzf.h>
typedef CGAL::Gmpzf ET;
#else
#include <CGAL/MP_Float.h>
typedef CGAL::MP_Float ET;
#endif

typedef CGAL::Homogeneous<ET>                 K;
typedef K::Point_3                            Point_3;
typedef CGAL::Polytope_distance_d_traits_3<K> Traits;
typedef CGAL::Polytope_distance_d<Traits>     Polytope_distance;

namespace kcadl{
struct MinGeometry{
	double minDist = 100; // default maximum distance
	std::vector<int> nearSegment1{0};
	std::vector<int> nearSegment2{0};
};

class ChainGeometry {

public:
	ChainGeometry();
	virtual ~ChainGeometry();

	std::vector<SegmentGeometry> Segments;

	/**
	An adaptive minimum distance algorithm to determine the distance between two chain geometries. Iteratively
	narrows in on segments with minimum distance.
	\param chainGeometry2 chain geometry object
	\param allowDist the allowable distance to break computations early
	\return minGeometry minimum geometry object
	 */
	MinGeometry minDist(ChainGeometry &chainGeometry, double allowDist);

	/**
	Add a segment geometry to the chain geometry
	 */
	void addSegment(const SegmentGeometry &segmentGeometry);

private:
};

/**
Calculate the minimum distance between two sets of points.
 */
double minDistPointLists(std::vector<Point_3> &pointList1, std::vector<Point_3> &pointList2, double &rad1, double &rad2);

} // namespace kcadl

#endif /* SRC_CHAINGEOMETRY_H_ */
