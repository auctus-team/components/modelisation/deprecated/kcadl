/*
 * This software is the property of Auctus, the INRIA research team.
 * Please refer to the Licence file of the repository and Authors
 */

/**
 * @file ExponentialMatrices.cpp
 *
 * @brief Class for generating an exponential interval matrix.
 *
 * @ingroup ExponentialMatrices
 *
 * @author Joshua Pickard
 * Contact: joshua.pickard@inria.fr
 *
 */

#include "ExponentialMatrices.h"
#include "ibex.h"

namespace kcadl{
ExponentialMatrices::ExponentialMatrices() {
	// TODO Auto-generated constructor stub

}

ExponentialMatrices::~ExponentialMatrices() {
	// TODO Auto-generated destructor stub
}
}
