/*
 * This software is the property of Auctus, the INRIA research team.
 * Please refer to the Licence file of the repository and Authors
 */

/**
 * @defgroup Utilities Utilities
 *
 * @brief Useful routines.
 *
 * @author Joshua Pickard
 * Contact: joshua.pickard@inria.fr
 *
 */

#ifndef SRC_UTILITIES_H_
#define SRC_UTILITIES_H_

#include "ibex.h"

namespace ibex {

	ibex::IntervalVector flatten(const ibex::IntervalMatrix& M);
	ibex::IntervalMatrix unflatten(const ibex::IntervalVector& V, int row, int col);

	ibex::Interval norm(const ibex::IntervalVector& V);

	// Outputs
	void print_intlab_vector(const ibex::IntervalVector &V);
	void print_intlab_vector(const ibex::Vector &V);
	void print_intlab_matrix(const ibex::IntervalMatrix &A);
	void print_intlab_matrix(const ibex::Matrix &A);
	void writeBox(std::ofstream &os, ibex::IntervalVector &box);
}

#endif /* SRC_UTILITIES_H_ */
