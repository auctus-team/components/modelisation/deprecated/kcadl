/*
 * This software is the property of Auctus, the INRIA research team.
 * Please refer to the Licence file of the repository and Authors
 */

/**
 * @file Utilities.cpp
 *
 * @brief Useful routines.
 *
 * @ingroup Utilities
 *
 * @author Joshua Pickard
 * Contact: joshua.pickard@inria.fr
 *
 */

#include "Utilities.h"
#include "ibex.h"

namespace ibex {

	ibex::IntervalVector flatten(const ibex::IntervalMatrix& M){
		ibex::IntervalVector flat(M.nb_cols() * M.nb_rows());
		for (int j=0;j<M.nb_cols();j++)
			for (int i=0;i<M.nb_rows();i++)
				flat[j*(M.nb_rows())+i] = M[i][j];
		return flat;
	}

	ibex::IntervalMatrix unflatten(const ibex::IntervalVector& V, int row, int col){
		ibex::IntervalMatrix M(row,col);
		for (int i=0;i<M.nb_rows();i++)
			for (int j=0;j<M.nb_cols();j++)
				M[i][j] = V[j*(M.nb_rows())+i];
		return M;
	}

	ibex::Interval norm(const ibex::IntervalVector& V){
		ibex::Interval norm_val = 0;
		for (int i=0; i<V.size(); i++){
			norm_val += sqr(V[i]);
		}
		norm_val = sqrt(norm_val);
		return norm_val;
	}

	void print_intlab_vector(const ibex::IntervalVector &V){
		for (int i=0; i<V.size(); i++){
			std::cout << "infsup(" << V[i].lb() << ',' << V[i].ub() << "),\t";
		}
		std::cout << ";\n";
	}

	void print_intlab_vector(const ibex::Vector &V){
		for (int i=0; i<V.size(); i++){
			std::cout << V[i] << ",\t";
		}
		std::cout << ";\n";
	}

	void print_intlab_matrix(const ibex::IntervalMatrix &A){
		for (int i=0; i<A.nb_rows(); i++){
			for (int j=0; j<A.nb_cols(); j++){
				std::cout << "infsup(" << A[i][j].lb() << ',' << A[i][j].ub() << "),\t";
			}
			std::cout << ";\n";
		}
	}

	void print_intlab_matrix(const ibex::Matrix &A){
		for (int i=0; i<A.nb_rows(); i++){
			for (int j=0; j<A.nb_cols(); j++){
				std::cout << A[i][j] << ",\t";
			}
			std::cout << ";\n";
		}
	}

	void writeBox(std::ofstream &os, ibex::IntervalVector &box){
		os << box[0].lb() << '\t' << box[0].ub() << '\t' << box[1].lb() << '\t' << box[1].ub() << '\t' << box[2].lb() << '\t' << box[2].ub() << '\t';
	}

}


