/*
 * This software is the property of Auctus, the INRIA research team.
 * Please refer to the Licence file of the repository and Authors
 */

/**
 * @defgroup SegmentGeometry SegmentGeometry
 *
 * @brief Class for generating geometric segments for building imprecise serial kinematic chains.
 *
 * @author Joshua Pickard
 * Contact: joshua.pickard@inria.fr
 *
 */

#ifndef SRC_SEGMENTGEOMETRY_H_
#define SRC_SEGMENTGEOMETRY_H_

#include <CGAL/Homogeneous.h>
#include <CGAL/Polytope_distance_d.h>
#include <CGAL/Polytope_distance_d_traits_3.h>
#include "ibex.h"

#ifdef CGAL_USE_GMP
#include <CGAL/Gmpzf.h>
typedef CGAL::Gmpzf ET;
#else
#include <CGAL/MP_Float.h>
typedef CGAL::MP_Float ET;
#endif

typedef CGAL::Homogeneous<ET>                 K;
typedef K::Point_3                            Point_3;
typedef CGAL::Polytope_distance_d_traits_3<K> Traits;
typedef CGAL::Polytope_distance_d<Traits>     Polytope_distance;

namespace kcadl{

class SegmentGeometry {
	friend class ChainGeometry;

public:
	SegmentGeometry(ibex::IntervalVector &startBox, ibex::IntervalVector &endBox, double cylinderRadius);
	virtual ~SegmentGeometry();

	/**
	The minimum distance between two segments geometries
	 */
	double minDist(const SegmentGeometry &segmentGeometry);

	ibex::IntervalVector start_box();
	ibex::IntervalVector end_box();


private:
	double radius = 0; /**< radius defining the cylinder and hemispherical ends */
	std::vector<Point_3> pointList; /**< the set of points to obtain the convex hull */
	ibex::IntervalVector sBox; /**< the interval vector of the segment start points */
	ibex::IntervalVector eBox; /**< the interval vector of the segment end points */

	/**
	Append the set of vertices associated with an interval vector to a list
	 */
	void VertexPoints(ibex::IntervalVector &box, std::vector<Point_3> &list);
};
} // namespace kcadl

#endif /* SRC_SEGMENTGEOMETRY_H_ */
