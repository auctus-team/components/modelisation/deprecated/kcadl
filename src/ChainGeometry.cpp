/*
 * This software is the property of Auctus, the INRIA research team.
 * Please refer to the Licence file of the repository and Authors
 */

/**
 * @file ChainGeometry.cpp
 *
 * @brief Class for generating imprecise serial kinematic chains from SegmentGeometry objects.
 *
 * @ingroup ChainGeometry
 *
 * @author Joshua Pickard
 * Contact: joshua.pickard@inria.fr
 *
 */

#include "ChainGeometry.h"
#include <algorithm>

#if defined(_OPENMP)
#include <omp.h>
#endif

namespace kcadl{
ChainGeometry::ChainGeometry() {
	// TODO Auto-generated constructor stub

}

ChainGeometry::~ChainGeometry() {
	// TODO Auto-generated destructor stub
}

void ChainGeometry::addSegment(const SegmentGeometry &segmentGeometry){
	Segments.push_back(segmentGeometry);
}

MinGeometry ChainGeometry::minDist(ChainGeometry &chainGeometry, double allowDist){
	MinGeometry minGeometry;

	bool adaptive = false;
	bool returnwhenzero = false;

	if (adaptive){
		// Determine distance between convex hulls of chainGeometry1 and chainGeometry2 and check if
		// distance is greater than allowDist. If so, distance is safe and exit. Worst case must check
		// all combinations of convex segments to determine the distance.
		std::vector<int> pointList1_segments_;
		std::vector<int> pointList2_segments_;
		for (int iSegment=0; iSegment<Segments.size(); iSegment++) pointList1_segments_.push_back(iSegment);
		for (int iSegment=0; iSegment<chainGeometry.Segments.size(); iSegment++) pointList2_segments_.push_back(iSegment);

		// Make lists for bisection
		std::deque<std::pair<std::pair<std::vector<int>,std::vector<int>>, double>> list_segments;
		std::pair<std::vector<int>,std::vector<int>> segments_indices_pair_;
		segments_indices_pair_.first = pointList1_segments_;
		segments_indices_pair_.second = pointList2_segments_;
		std::pair<std::pair<std::vector<int>,std::vector<int>>, double> segments_indices_dist_pair_;
		segments_indices_dist_pair_.first = segments_indices_pair_;
		segments_indices_dist_pair_.second = 1000;
		list_segments.push_back(segments_indices_dist_pair_);

		std::vector<double> minDists;
		// Continue while lists have elements
		while(list_segments.size()>0){
	//		std::cout << "Chain1: " << list_segments.front().first.first.size() << '\t' << list_segments.front().first.first.front() << '\t' << list_segments.front().first.first.back() << std::endl;
	//		std::cout << "Chain2: " << list_segments.front().first.second.size() << '\t' << list_segments.front().first.second.front() << '\t' << list_segments.front().first.second.back() << std::endl;

			// Get unions of pointLists and max radii from the lists of segments
			std::vector<Point_3> pointList1;
			std::vector<Point_3> pointList2;
			double rad1=0;
			double rad2=0;
			for (int iSegment=list_segments.front().first.first.front(); iSegment<=list_segments.front().first.first.back(); iSegment++){
				pointList1.insert(pointList1.end(),
						Segments[iSegment].pointList.begin(),
						Segments[iSegment].pointList.end());
				if (Segments[iSegment].radius>rad1)
					rad1 = Segments[iSegment].radius;
			}
			for (int iSegment=list_segments.front().first.second.front(); iSegment<=list_segments.front().first.second.back(); iSegment++){
				pointList2.insert(pointList2.end(),
						chainGeometry.Segments[iSegment].pointList.begin(),
						chainGeometry.Segments[iSegment].pointList.end());
				if (chainGeometry.Segments[iSegment].radius>rad2)
					rad2 = chainGeometry.Segments[iSegment].radius;
			}
			// Get distance between convex hulls
			double _dist = minDistPointLists(pointList1, pointList2, rad1, rad2);

			// Exit early
			if (returnwhenzero && _dist == 0 && list_segments.front().first.first.size()==1 && list_segments.front().first.second.size()==1){
				minGeometry.minDist = _dist;
				minGeometry.nearSegment1.clear();
				for (int iSegment=list_segments.front().first.first.front(); iSegment<=list_segments.front().first.first.back(); iSegment++)
					minGeometry.nearSegment1.push_back(iSegment);
				minGeometry.nearSegment2.clear();
				for (int iSegment=list_segments.front().first.second.front(); iSegment<=list_segments.front().first.second.back(); iSegment++)
					minGeometry.nearSegment2.push_back(iSegment);
			} else if (_dist > allowDist){
				if (list_segments.size()==1){
	//				std::cout << "SAFE distance - all segments" << std::endl;
					minGeometry.minDist = _dist;
					minGeometry.nearSegment1.clear();
					minGeometry.nearSegment2.clear();
					minGeometry.nearSegment1.push_back(-1);
					minGeometry.nearSegment2.push_back(-1);
					return minGeometry;
				}
	//			std::cout << "SAFE distance - removing segment pair" << std::endl;
				list_segments.pop_front();
			} else {
				// Bisection
				if (list_segments.front().first.first.size() > 1 || list_segments.front().first.second.size() > 1){
					std::deque<std::pair<std::pair<std::vector<int>,std::vector<int>>, double>> temporary_list_segments;
					// Chain 1
					if (list_segments.front().first.first.size() > 1){
	//					std::cout << "bisection chain1" << std::endl;
						int stpt = list_segments.front().first.first.front();
						int endpt = list_segments.front().first.first.back();
						int midpt1 = stpt;
						int midpt2 = endpt;
						if (endpt-stpt > 1){
							midpt1 = stpt + round((endpt-stpt)/2);
							midpt2 = midpt1;
						}
	//					std::cout << '[' << stpt << '\t' << midpt1 << "]\t[" << midpt2 << '\t'<< endpt << ']' << std::endl;

						// b1
	//					std::cout << "bisect1 " << std::endl;
						std::vector<int> pointList_segments_b1;
						for (int iSegment=stpt; iSegment<=midpt1; iSegment++)
							pointList_segments_b1.push_back(iSegment);
						std::pair<std::vector<int>,std::vector<int>> segments_indices_pair1;
						segments_indices_pair1.first = pointList_segments_b1;
						segments_indices_pair1.second = list_segments.front().first.second;
						std::pair<std::pair<std::vector<int>,std::vector<int>>, double> segments_indices_dist_pair1;
						segments_indices_dist_pair1.first = segments_indices_pair1;
						segments_indices_dist_pair1.second = _dist;
						temporary_list_segments.push_back(segments_indices_dist_pair1);

						// b2
	//					std::cout << "bisect2 " << std::endl;
						std::vector<int> pointList_segments_b2;
						for (int iSegment=midpt2; iSegment<=endpt; iSegment++)
							pointList_segments_b2.push_back(iSegment);
						std::pair<std::vector<int>,std::vector<int>> segments_indices_pair2;
						segments_indices_pair2.first = pointList_segments_b2;
						segments_indices_pair2.second = list_segments.front().first.second;
						std::pair<std::pair<std::vector<int>,std::vector<int>>, double> segments_indices_dist_pair2;
						segments_indices_dist_pair2.first = segments_indices_pair2;
						segments_indices_dist_pair2.second = _dist;
						temporary_list_segments.push_back(segments_indices_dist_pair2);
					}
					// Chain 2
					if (list_segments.front().first.second.size() > 1){
	//					std::cout << "bisection chain2" << std::endl;
						if (temporary_list_segments.size()==0)
							temporary_list_segments.push_back(list_segments.front());
						while (temporary_list_segments.size()>0){
							int stpt = temporary_list_segments.front().first.second.front();
							int endpt = temporary_list_segments.front().first.second.back();
							int midpt1 = stpt;
							int midpt2 = endpt;
							if (endpt-stpt > 1){
								midpt1 = stpt + round((endpt-stpt)/2);
								midpt2 = midpt1;
							}
	//						std::cout << '[' << stpt << '\t' << midpt1 << "]\t[" << midpt2 << '\t'<< endpt << ']' << std::endl;

							// b1
	//						std::cout << "bisect1 " << std::endl;
							std::vector<int> pointList_segments_b1;
							for (int iSegment=stpt; iSegment<=midpt1; iSegment++)
								pointList_segments_b1.push_back(iSegment);
							std::pair<std::vector<int>,std::vector<int>> segments_indices_pair1;
							segments_indices_pair1.first = temporary_list_segments.front().first.first;
							segments_indices_pair1.second = pointList_segments_b1;
							std::pair<std::pair<std::vector<int>,std::vector<int>>, double> segments_indices_dist_pair1;
							segments_indices_dist_pair1.first = segments_indices_pair1;
							segments_indices_dist_pair1.second = _dist;
							list_segments.push_back(segments_indices_dist_pair1);

							// b2
	//						std::cout << "bisect2 " << std::endl;
							std::vector<int> pointList_segments_b2;
							for (int iSegment=midpt2; iSegment<=endpt; iSegment++)
								pointList_segments_b2.push_back(iSegment);
							std::pair<std::vector<int>,std::vector<int>> segments_indices_pair2;
							segments_indices_pair2.first = temporary_list_segments.front().first.first;
							segments_indices_pair2.second = pointList_segments_b2;
							std::pair<std::pair<std::vector<int>,std::vector<int>>, double> segments_indices_dist_pair2;
							segments_indices_dist_pair2.first = segments_indices_pair2;
							segments_indices_dist_pair2.second = _dist;
							list_segments.push_back(segments_indices_dist_pair2);
							temporary_list_segments.pop_front();
						}
					} else {
						for (int iSegment=0; iSegment<temporary_list_segments.size(); iSegment++){
							list_segments.push_back(temporary_list_segments[iSegment]);
						}
					}
				} else {
					// Update minGeometry
					if (_dist < minGeometry.minDist){
						minGeometry.minDist = _dist;
						minGeometry.nearSegment1.clear();
						for (int iSegment=list_segments.front().first.first.front(); iSegment<=list_segments.front().first.first.back(); iSegment++)
							minGeometry.nearSegment1.push_back(iSegment);
						minGeometry.nearSegment2.clear();
						for (int iSegment=list_segments.front().first.second.front(); iSegment<=list_segments.front().first.second.back(); iSegment++)
							minGeometry.nearSegment2.push_back(iSegment);
					}
				}
				list_segments.pop_front();
			}
		}
	} else {
		// Check all combinations of convex segment geometries to determine the nearest geometries
		// and their distance.
		std::vector<std::pair<int,int>> listSegments;
		for (int iSegment2=chainGeometry.Segments.size()-1; iSegment2>=0; iSegment2--){
			for (int iSegment1=Segments.size()-1; iSegment1>=0; iSegment1--){
				std::pair<int,int> pairSegments;
				pairSegments.first = iSegment1;
				pairSegments.second = iSegment2;
				listSegments.push_back(pairSegments);
			}
		}

		for (int i=0; i<listSegments.size(); i++){
			double _dist = minDistPointLists(Segments[listSegments[i].first].pointList, chainGeometry.Segments[listSegments[i].second].pointList,
					Segments[listSegments[i].first].radius, chainGeometry.Segments[listSegments[i].second].radius);

			// Update minGeometry
			if (_dist == 0){
				if (returnwhenzero){
					minGeometry.minDist = _dist;
					minGeometry.nearSegment1.clear();
					minGeometry.nearSegment1.push_back(listSegments[i].first);
					minGeometry.nearSegment2.clear();
					minGeometry.nearSegment2.push_back(listSegments[i].second);
				}
			} else
			if (_dist < minGeometry.minDist){
				minGeometry.minDist = _dist;
				minGeometry.nearSegment1.clear();
				minGeometry.nearSegment1.push_back(listSegments[i].first);
				minGeometry.nearSegment2.clear();
				minGeometry.nearSegment2.push_back(listSegments[i].second);
			} else if (_dist == minGeometry.minDist){
				// Add new segments not already in lists
				if(std::find(minGeometry.nearSegment1.begin(), minGeometry.nearSegment1.end(), listSegments[i].first) == minGeometry.nearSegment1.end())
					minGeometry.nearSegment1.push_back(listSegments[i].first);
				if(std::find(minGeometry.nearSegment2.begin(), minGeometry.nearSegment2.end(), listSegments[i].second) == minGeometry.nearSegment2.end())
					minGeometry.nearSegment2.push_back(listSegments[i].second);
			}
		}
	}
	return minGeometry;
}

double minDistPointLists(std::vector<Point_3> &pointList1, std::vector<Point_3> &pointList2, double &rad1, double &rad2){

	// Make lists unique
	sort(pointList1.begin(), pointList1.end());
	pointList1.erase(unique(pointList1.begin(), pointList1.end() ), pointList1.end());
	sort(pointList2.begin(), pointList2.end());
	pointList2.erase(unique(pointList2.begin(), pointList2.end() ), pointList2.end());

	Polytope_distance pd(pointList1.begin(), pointList1.end(), pointList2.begin(), pointList2.end());
	assert (pd.is_valid());

	// get squared distance
	double dist = sqrt(CGAL::to_double (pd.squared_distance_numerator()) /
		    CGAL::to_double (pd.squared_distance_denominator()));
	dist = std::max(0.0, dist - rad1 - rad2);

	return dist;
}

} // namespace kcadl

