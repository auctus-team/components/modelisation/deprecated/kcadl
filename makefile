SOURCE=src/
VIBESSOURCE=/opt/kcadl/3rd/vibes/client-api/C++/src/
EXAMPLES=examples/
EXAMPLES_FK=examples/Forward-Kinematics/
EXAMPLES_IK=examples/Inverse-Kinematics/
EXAMPLES_PRED=examples/Movement-Prediction/
EXAMPLES_IA=examples/Interval-Analysis/
BINS=bin/
SRCS=$(SOURCE)Parameters.cpp $(SOURCE)KinematicChain.cpp $(SOURCE)Twist.cpp $(VIBESSOURCE)vibes.cpp $(SOURCE)Joint.cpp $(SOURCE)Frame.cpp $(SOURCE)Segment.cpp $(SOURCE)Utilities.cpp $(SOURCE)LinearAlgebra.cpp
GEOM_SRCS=$(SOURCE)SegmentGeometry.cpp $(SOURCE)ChainGeometry.cpp
LIBS	 := -L /opt/ros/melodic/lib -L catkin_ws/devel/lib/human_arm_redundancy_modelling -L  /opt/kcadl/3rd/ibex-lib/build/lib -lboost_thread -lm -lmpfr -lgmp -libex -lgaol -lgdtoa -lultim -lsoplex -lz
INC      := -I $(SOURCE) -I /opt/kcadl/3rd/ibex-lib/build/include -I /opt/kcadl/3rd/ibex-lib/build/include/ibex -I /opt/kcadl/3rd/ibex-lib/build/include/ibex/3rd -I /opt/kcadl/3rd/vibes/client-api/C++/src
GEOM_LIBS:= -L /opt/kcadl/3rd/CGAL-4.13.1/build/lib -L /opt/kcadl/3rd/qhull/lib -lCGAL -lqhullcpp -lqhullstatic_r
GEOM_INC := -I /opt/kcadl/3rd/CGAL-4.13.1/build/include -I /opt/kcadl/3rd/qhull/src

ifdef COVERAGE
        CXXFLAGS := -O3 -std=c++11 -frounding-math --coverage
else
        CXXFLAGS := -O3 -std=c++11 -frounding-math
endif

examples_FK: BarrettTech_WAM_FK Human_7dof_upperlimb_FK Human_7dof_upperlimb_FK2 Franka_Panda_FK UR5_FK Kuka_LBR_iiwa_FK Scara_FK
examples_IK: Planar_IK Redundant_Planar_IK Redundant_Planar_IK2 Spatial_IK Puma560_IK Porta_6_dof_manipulator_IK Human_7dof_upperlimb_IK
examples_IA: EvalExponentialMatrix
examples: examples_FK examples_IK examples_IA

BarrettTech_WAM_FK: $(EXAMPLES_FK)BarrettTech_WAM_FK.cpp
	$(CXX) $(INC) $(CXXFLAGS) -o $(BINS)$@ $(EXAMPLES_FK)$@.cpp $(SRCS) $(LIBS)
Human_7dof_upperlimb_FK: $(EXAMPLES_FK)Human_7dof_upperlimb_FK.cpp
	$(CXX) $(INC) $(CXXFLAGS) -o $(BINS)$@ $(EXAMPLES_FK)$@.cpp $(SRCS) $(LIBS)
Human_7dof_upperlimb_FK2: $(EXAMPLES_FK)Human_7dof_upperlimb_FK2.cpp
	$(CXX) $(INC) $(CXXFLAGS) -o $(BINS)$@ $(EXAMPLES_FK)$@.cpp $(SRCS) $(LIBS)
Franka_Panda_FK: $(EXAMPLES_FK)Franka_Panda_FK.cpp
	$(CXX) $(INC) $(CXXFLAGS) -o $(BINS)$@ $(EXAMPLES_FK)$@.cpp $(SRCS) $(LIBS)
UR5_FK: $(EXAMPLES_FK)UR5_FK.cpp
	$(CXX) $(INC) $(CXXFLAGS) -o $(BINS)$@ $(EXAMPLES_FK)$@.cpp $(SRCS) $(LIBS)
Kuka_LBR_iiwa_FK: $(EXAMPLES_FK)Kuka_LBR_iiwa_FK.cpp
	$(CXX) $(INC) $(CXXFLAGS) -o $(BINS)$@ $(EXAMPLES_FK)$@.cpp $(SRCS) $(LIBS)
Scara_FK: $(EXAMPLES_FK)Scara_FK.cpp
	$(CXX) $(INC) $(CXXFLAGS) -o $(BINS)$@ $(EXAMPLES_FK)$@.cpp $(SRCS) $(LIBS)

Planar_IK: $(EXAMPLES_IK)Planar_IK.cpp
	$(CXX) $(INC) $(CXXFLAGS) -o $(BINS)$@ $(EXAMPLES_IK)$@.cpp $(SRCS) $(LIBS)
Redundant_Planar_IK: $(EXAMPLES_IK)Redundant_Planar_IK.cpp
	$(CXX) $(INC) $(CXXFLAGS) -o $(BINS)$@ $(EXAMPLES_IK)$@.cpp $(SRCS) $(LIBS)
Redundant_Planar_IK2: $(EXAMPLES_IK)Redundant_Planar_IK2.cpp
	$(CXX) $(INC) $(CXXFLAGS) -o $(BINS)$@ $(EXAMPLES_IK)$@.cpp $(SRCS) $(LIBS)
Spatial_IK: $(EXAMPLES_IK)Spatial_IK.cpp
	$(CXX) $(INC) $(CXXFLAGS) -o $(BINS)$@ $(EXAMPLES_IK)$@.cpp $(SRCS) $(LIBS)
Puma560_IK: $(EXAMPLES_IK)Puma560_IK.cpp
	$(CXX) $(INC) $(CXXFLAGS) -o $(BINS)$@ $(EXAMPLES_IK)$@.cpp $(SRCS) $(LIBS)
Porta_6_dof_manipulator_IK: $(EXAMPLES_IK)Porta_6_dof_manipulator_IK.cpp
	$(CXX) $(INC) $(CXXFLAGS) -o $(BINS)$@ $(EXAMPLES_IK)$@.cpp $(SRCS) $(LIBS)
Human_7dof_upperlimb_IK: $(EXAMPLES_IK)Human_7dof_upperlimb_IK.cpp
	$(CXX) $(INC) $(CXXFLAGS) -o $(BINS)$@ $(EXAMPLES_IK)$@.cpp $(SRCS) $(LIBS)
	
Franka_Movement_Prediction: $(EXAMPLES_PRED)Franka_Movement_Prediction.cpp
	$(CXX) $(INC) $(GEOM_INC) $(CXXFLAGS) -o $(BINS)$@ $(EXAMPLES_PRED)$@.cpp $(SRCS) $(GEOM_SRCS) $(LIBS) $(GEOM_LIBS)
Human_Arm_Movement_Prediction: $(EXAMPLES_PRED)Human_Arm_Movement_Prediction.cpp
	$(CXX) $(INC) $(GEOM_INC) $(CXXFLAGS) -o $(BINS)$@ $(EXAMPLES_PRED)$@.cpp $(SRCS) $(GEOM_SRCS) $(LIBS) $(GEOM_LIBS)

EvalExponentialMatrix: $(EXAMPLES_IA)EvalExponentialMatrix.cpp
	$(CXX) $(INC) $(CXXFLAGS) -o $(BINS)$@ $(EXAMPLES_IA)$@.cpp $(SRCS) $(LIBS)		
	
clean:
	rm -f $(BINS)*
